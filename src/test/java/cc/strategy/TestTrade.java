package cc.strategy;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.Test;
import cc.strategy.Strategy.StrategyType;
import cc.model.Model.Decision;

/**
 * The Class TestTrade.
 * 
 * @author Jack Guo
 */
public class TestTrade {
	
	/** The factory. */
	private EntityManagerFactory factory;
	
	/** The em. */
	private EntityManager em;

	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		this.factory = Persistence.createEntityManagerFactory("oracle");
		this.em = factory.createEntityManager();
	}

	/**
	 * Test persist trade.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testPersistTrade() throws Exception {
		Trade trade = new Trade("Strat1_APPL_1", StrategyType.TwoMA, "APPL", 500, 20.4, Decision.BUY,
				LocalDateTime.now(), "Strat1");
		try {
			em.getTransaction().begin();
			em.persist(trade);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("duplicate");
			Trade foundTrade;
			em.getTransaction().begin();
			foundTrade = em.find(Trade.class, "Strat1_APPL_1");
			em.getTransaction().commit();

			assertEquals(500, foundTrade.getAmount());
		}

	}
}
