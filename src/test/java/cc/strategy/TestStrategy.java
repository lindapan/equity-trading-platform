package cc.strategy;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;

import cc.listener.MarketDataHandler;
import cc.monitors.StrategyMonitor;

/**
 * The Class TestStrategy.
 * 
 * @author Jack Guo
 */
public class TestStrategy {

	/** The id. */
	private String id;
	
	/** The type. */
	private String type;
	
	/** The stock name. */
	private String stockName;
	
	/** The Two MA parameters. */
	private List<Double> TwoMAParameters;
	
	/** The Bollinger parameters. */
	private List<Double> BollingerParameters;
	
	/** The stopping threshold. */
	private Double stoppingThreshold;
	
	/** The exit threshold. */
	private Double exitThreshold;
	
	/** The amount. */
	private int amount;
	
	/** The strategy. */
	private Strategy strategy;
	
	/** The sm. */
	private StrategyMonitor sm;
	
	/** The mdh. */
	private MarketDataHandler mdh;
	
	/** The stock name 2. */
	private String stockName2;
	
	/** The id 2. */
	private String id2;
	
	/** The subscribers. */
	HashMap<String, LinkedList<Strategy>> subscribers;

	/**
	 * Inits the.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void init() throws Exception {
		sm = StrategyMonitor.getInstance();
		mdh = MarketDataHandler.getInstance();
		subscribers = mdh.getSubscribers();
		id = "strat100";
		id2 = "strat200";
		type = "TwoMA";
		stockName = "MSFT";
		stockName2 = "APPL";

		TwoMAParameters = new LinkedList<Double>();
		BollingerParameters = new LinkedList<Double>();
		
		TwoMAParameters.add(20.0);
		TwoMAParameters.add(2.0);
		BollingerParameters.add(20.0);
		BollingerParameters.add(0.3);
		stoppingThreshold = 0.1;
		exitThreshold = 0.05;
		amount = 5000;
		sm.createStrategy(id, stockName, type, TwoMAParameters, stoppingThreshold, exitThreshold, amount);
		
		sm.createStrategy(id2, stockName2, "BollingerBands", BollingerParameters, stoppingThreshold, exitThreshold, amount);
		
		//mdh.run();

	}

//	@Test
//	public void testStartAndPauseStrategy() {
//	sm.createStrategy(id, stockName, type, TwoMAParameters, stoppingThreshold, exitThreshold, amount);
	
//	sm.createStrategy(id2, stockName2, "BollingerBands", BollingerParameters, stoppingThreshold, exitThreshold, amount);
//		assertEquals("strat100", subscribers.get(stockName).get(0).getId());
//		assertEquals("strat200", subscribers.get(stockName2).get(0).getId());
//		sm.getStrategy(id).pause();
//		assertEquals(null, subscribers.get(stockName));
//		assertEquals("STOPPED", sm.getStrategy(id).getStatus().toString());
//		sm.getStrategy(id2).pause();
//		assertEquals(null, subscribers.get(stockName2));
//		sm.getStrategy(id).start();
//		assertEquals(id, subscribers.get(stockName).get(0).getId());
//		assertEquals("RUNNING", sm.getStrategy(id).getStatus().toString());
//		
//	}
	
/**
 * Test make trade.
 *
 * @throws Exception the exception
 */
//	@Test
	public void testMakeTrade() throws Exception {
		Strategy s1 = sm.getStrategy(id);
		s1.makeTrade(); 
		Queue<Trade> et = s1.getExecutedTrades();
		assertEquals(1, et.size());
		
	}
	
	/**
	 * Test get profit history.
	 */
	@Test
	public void testGetProfitHistory() {
		Strategy s1 = sm.getStrategyFromDB(id);
		try {
			List<Double> history = s1.getProfitOrLossHistory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Test.
	 */
	//@Test
	public void test() {
		
	}

}
