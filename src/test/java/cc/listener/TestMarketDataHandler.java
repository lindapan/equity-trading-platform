package cc.listener;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import cc.stock.Stock;
import cc.strategy.Strategy;

/**
 * The Class TestMarketDataHandler.
 * 
 * @author Jack Guo
 */
public class TestMarketDataHandler {
	
	/** The mdh. */
	private MarketDataHandler mdh;
	
	/** The stock name. */
	private String stockName;

	/**
	 * Inits the.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void init() throws Exception {
		stockName = "MSFT";
		mdh = MarketDataHandler.getInstance();
	}

	/**
	 * Test get prices from API.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetPricesFromAPI() throws Exception {
		Stock msft = MarketDataHandler.getStockFromAPI(stockName);
		assertEquals(120, msft.getClose().size());
		assertEquals(stockName, msft.getListing());
		assertEquals(120, msft.getTimeStamp().size());
	}

	/**
	 * Test subscribers.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testSubscribers() throws Exception {
		LinkedList<Double> params1 = new LinkedList<Double>();
		params1.add(20.0);
		params1.add(3.0);
		Strategy test1 = new Strategy("Strat1", "TwoMA", stockName, params1, 0.05, 0.3, 5000);
		Strategy test2 = new Strategy("Strat2", "TwoMA", stockName, params1, 0.05, 0.3, 5000);
		HashMap<String, LinkedList<Strategy>> subscribers = mdh.getSubscribers();
		
		
		assertEquals(stockName, subscribers.get(stockName).get(0).getStockName());
		assertEquals("Strat2", subscribers.get(stockName).get(1).getId());
		test1.unsubscribeStrategy();
		assertEquals("Strat2", subscribers.get(stockName).get(0).getId());
		mdh.clearSubscribers();
		assertEquals(0, subscribers.size());
		test1.subscribeStrategy();
		assertEquals(1, subscribers.size());
	}
	
	/**
	 * Test subscribers diff stocks.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testSubscribersDiffStocks() throws Exception {
		mdh.clearSubscribers();
		LinkedList<Double> params1 = new LinkedList<Double>();
		params1.add(20.0);
		params1.add(3.0);
		Strategy test1 = new Strategy("Strat1", "TwoMA", stockName, params1, 0.05, 0.3, 5000);
		Strategy test2 = new Strategy("Strat2", "TwoMA", stockName, params1, 0.05, 0.3, 5000);
		Strategy test3 = new Strategy("Strat3", "TwoMA", "GOOG", params1, 0.05, 0.3, 5000);
		HashMap<String, LinkedList<Strategy>> subscribers = mdh.getSubscribers();
		
		assertEquals(stockName, subscribers.get(stockName).get(0).getStockName());
		assertEquals("Strat2", subscribers.get(stockName).get(1).getId());
		assertEquals("Strat3", subscribers.get("GOOG").get(0).getId());
		test1.unsubscribeStrategy();
		assertEquals("Strat2", subscribers.get(stockName).get(0).getId());
		test2.unsubscribeStrategy();
		assertEquals("Strat3", subscribers.get("GOOG").get(0).getId());
		mdh.clearSubscribers();
		assertEquals(0, subscribers.size());
		test3.subscribeStrategy();
		assertEquals(1, subscribers.size());
	}


}
