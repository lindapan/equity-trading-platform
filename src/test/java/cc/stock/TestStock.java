package cc.stock;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * The Class TestStock.
 * 
 * @author Jack Guo
 */
public class TestStock {
	
	/** The high price. */
	public List<Double> highPrice;
	
	/** The low price. */
	public List<Double> lowPrice;
	
	/** The open. */
	public List<Double> open;
	
	/** The close. */
	public List<Double> close;
	
	/** The volume. */
	public List<Integer> volume;
	
	/** The time stamp. */
	public List<LocalDateTime> timeStamp;
	
	/** The stopping threshold. */
	public double stoppingThreshold;
	
	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		
		highPrice = new LinkedList<Double>();
		lowPrice = new LinkedList<Double>();
		open = new LinkedList<Double>();
		close = new LinkedList<Double>();
		volume = new LinkedList<Integer>();
		timeStamp = new LinkedList<LocalDateTime>();
		
		highPrice.add(1001.0);
		highPrice.add(1002.0);
		highPrice.add(1003.0);
		
		lowPrice.add(998.0);
		lowPrice.add(999.0);
		lowPrice.add(1000.0);
		
		open.add(998.5);
		open.add(999.5);
		open.add(1002.5);
		
		close.add(999.5);
		close.add(1001.5);
		close.add(1000.5);
		
		volume.add(13433);
		volume.add(18543);
		volume.add(49321);
		
		timeStamp.add(LocalDateTime.now());
		timeStamp.add(LocalDateTime.now());
		timeStamp.add(LocalDateTime.now());
	
	}

	/**
	 * Test.
	 */
	@Test
	public void test() {
		Stock stock = new Stock("AAPL", highPrice, lowPrice, open, close, volume, timeStamp);
		
		assertEquals(stock.getListing(), "AAPL");
		assertEquals(stock.getHighPrice(), highPrice);
		assertEquals(stock.getLowPrice(), lowPrice);
		assertEquals(stock.getOpen(), open);
		assertEquals(stock.getClose(), close);
		assertEquals(stock.getVolume(), volume);
		assertEquals(stock.getTimeStamp(), timeStamp);
		
		assertEquals(stock.getCurrPrice(), stock.avg(highPrice, lowPrice));
		
		
	}

}
