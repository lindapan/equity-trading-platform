package cc.monitors;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import cc.listener.MarketDataHandler;

/**
 * The Class TestStockMonitor.
 * 
 * @author Linda Pan
 */
public class TestStockMonitor {
	
	/** The stock 1. */
	public String stock1;
	
	/** The stock 2. */
	public String stock2;
	
	/** The stock 3. */
	public String stock3;
	
	/** The stock 4. */
	public String stock4;
	
	/** The period 1. */
	public int period1;
	
	/** The period 2. */
	public int period2;
	
	/** The period 3. */
	public int period3;
	
	/** The mdh. */
	public MarketDataHandler mdh;
	
	/** The sm. */
	public StockMonitor sm;
	
	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		stock1 = "MSFT";
		stock2 = "APPL";
		stock3 = "GOOG";
		stock4 = "asdjfhasjdfhwaeb12310";
		period1 = 20;
		period2 = 30;
		period3 = 300;
		mdh = MarketDataHandler.getInstance();
		
	}
	
	/**
	 * Test add stock.
	 */
	@Test
	public void testAddStock() {
		sm = StockMonitor.getInstance();
		sm.addStock(stock1);
		sm.addStock(stock2);
		assertEquals(2, sm.getNumberOfStocks());
		sm.addStock(stock3);
		assertEquals(3, sm.getNumberOfStocks());
		sm.removeAllStocks();
		
	}
	
	/**
	 * Test remove stock.
	 */
	@Test
	public void testRemoveStock() {
		sm = StockMonitor.getInstance();
		System.out.println(sm.getNumberOfStocks());
		sm.addStock(stock1);
		sm.addStock(stock2);
		sm.removeStock(stock1);
		assertEquals(1, sm.getNumberOfStocks());
		sm.addStock(stock3);
		assertEquals(2, sm.getNumberOfStocks());
		sm.removeAllStocks();
	}
	
	/**
	 * Test remove all stock.
	 */
	@Test
	public void testRemoveAllStock() {
		sm = StockMonitor.getInstance();
		sm.addStock(stock1);
		sm.addStock(stock2);
		sm.addStock(stock3);
		assertEquals(3, sm.getNumberOfStocks());
		sm.removeAllStocks();
		assertEquals(0, sm.getNumberOfStocks());
		sm.removeAllStocks();
	}
	
	/**
	 * Test get stock.
	 */
	@Test
	public void testGetStock() {
		sm = StockMonitor.getInstance();
		sm.addStock(stock1);
		sm.addStock(stock2);
		sm.addStock(stock3);
		assertEquals("MSFT", sm.getStock(stock1).getListing());
		sm.removeAllStocks();
	}
	
	/**
	 * Test get period.
	 */
	@Test
	public void testGetPeriod() {
		sm = StockMonitor.getInstance();
		sm.addStock(stock1, period1);
		sm.addStock(stock2, period2);
		sm.addStock(stock3, period3);
		assertEquals(period1, sm.getStock(stock1).getHighPrice().size());
		assertEquals(period2, sm.getStock(stock2).getLowPrice().size());
		assertEquals(period2, sm.getStock(stock2).getCurrPrice().size());
		sm.removeAllStocks();
	}
	
	/**
	 * Test get null stock.
	 */
	@Test
	public void testGetNullStock() {
		sm = StockMonitor.getInstance();
		sm.addStock(stock4);
		
		System.out.println(sm.getStock(stock4).getCurrPrice().size());
		sm.removeAllStocks();
	}
	
}
