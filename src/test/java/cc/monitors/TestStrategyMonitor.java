package cc.monitors;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import cc.listener.MarketDataHandler;
import cc.strategy.Strategy;

/**
 * The Class TestStrategyMonitor.
 * 
 * @author Jack Guo
 */
public class TestStrategyMonitor {

	/** The start param. */
	public List<Double> startParam;
	
	/** The stopping threshold. */
	public double stoppingThreshold;
	
	/** The exit threshold. */
	public double exitThreshold;
	
	/** The amount. */
	public int amount;
	
	/** The factory. */
	private EntityManagerFactory factory;
	
	/** The em. */
	private EntityManager em;
	
	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		startParam = new LinkedList<Double>();

		startParam.add(20.0);
		startParam.add(2.0);
//		startParam.add(3.0);

		stoppingThreshold = 0.5;
		exitThreshold = 0.3;
		amount = 5000;
		this.factory = Persistence.createEntityManagerFactory("oracle");
		this.em = factory.createEntityManager();
	}

//	@Test
//	public void testCreateStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		// Create a strategy
//		strategyMon.createStrategy("100", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		assertEquals(1, strategyMon.getAllStrategies().size());
//		assertEquals("100", strategyMon.getStrategy("100").getId());
//		strategyMon.removeAllStrategy();
//		assertEquals(0, strategyMon.getAllStrategies().size());
//	}
//
//	@Test
//	public void testRemoveStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		strategyMon.createStrategy("101", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("202", "GOOG", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.removeStrategy("101");
//		assertEquals(strategyMon.getStrategy("101"), null);
//	}
//
//	@Test
//	public void testRemoveAllStrategy() throws Exception {
//
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		// Create strategies
//		strategyMon.createStrategy("108", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("208", "GOOG", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("308", "WMT", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		// Remove all strategies
//		strategyMon.removeAllStrategy();
//		assertEquals(0, strategyMon.getAllStrategies().size());
//	}
//
//	@Test
//	public void testStartandPauseStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		strategyMon.createStrategy("103", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("203", "GOOG", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		// Start two strategies
//		strategyMon.startStrategy("103");
//		strategyMon.startStrategy("203");
//		assertEquals(2, strategyMon.getRunningStrategies().size());
//		// Pause one running strategy
//		strategyMon.pauseStrategy("103");
//		assertEquals(1, strategyMon.getRunningStrategies().size());
//		assertEquals(1, strategyMon.getPausedStrategies().size());
//	}
//
//	
//	@Test
//	public void testStartandStopAllStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		strategyMon.createStrategy("104", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("204", "GOOG", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("304", "WMT", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//
//		for (Strategy s : strategyMon.getAllStrategies()) {
//			System.out.println(s.getId());
//		}
//		// Start all strategy
//		strategyMon.startAllStrategy();
//		assertEquals(3, strategyMon.getRunningStrategies().size());
////		Stop all strategy
//		strategyMon.stopAllStrategy();
//		assertEquals(0, strategyMon.getRunningStrategies().size());
//
//	}
//
//	@Test
//	public void testPersistStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		strategyMon.createStrategy("105", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("205", "GOOG", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("305", "WMT", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//
//		Strategy foundStrategy;
//		em.getTransaction().begin();
//		foundStrategy = em.find(Strategy.class, "105");
//		assertEquals("AAPL", foundStrategy.getStockName());
//		em.getTransaction().commit();
//	}
//	
//	@Test
//	public void testStartStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		strategyMon.createStrategy("106", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		
//		assertEquals(Strategy.Status.RUNNING, strategyMon.getStrategy("106").getStatus());
//		strategyMon.pauseStrategy("106");
//		assertEquals(Strategy.Status.STOPPED, strategyMon.getStrategy("106").getStatus());
//		strategyMon.startStrategy("106");
//		assertEquals(Strategy.Status.RUNNING, strategyMon.getStrategy("106").getStatus());
//	}
//	
//	@Test
//	public void testPersistStatusUpdateStrategy() throws Exception {
//		StrategyMonitor strategyMon = StrategyMonitor.getInstance();
//		strategyMon.removeAllStrategy();
//		strategyMon.createStrategy("107", "AAPL", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//		strategyMon.createStrategy("207", "GOOG", "TwoMA", startParam, stoppingThreshold, exitThreshold, amount);
//
//		strategyMon.pauseStrategy("107");
//		Strategy foundStrategy;
//		em.getTransaction().begin();
//		foundStrategy = em.find(Strategy.class, "107");
//		assertEquals(Strategy.Status.STOPPED, strategyMon.getStrategy("107").getStatus());
//
//		foundStrategy = em.find(Strategy.class, "207");
//		assertEquals(Strategy.Status.RUNNING, foundStrategy.getStatus());
//		
//		em.getTransaction().commit();
//	}
	
	/**
 * Test get all strategy.
 *
 * @throws Exception the exception
 */
@Test
	public void testGetAllStrategy() throws Exception {
		StrategyMonitor sm = StrategyMonitor.getInstance();
		List<Strategy> rs = sm.getRunningStrategies();
		Gson gson = new Gson();
		String data = gson.toJson(rs);
		System.out.println(data);
		
//		for (Strategy i : rs) {
//			System.out.println(i.getId());
//		}
//		System.out.println(rs.size());
//		System.out.println("running");
//		System.out.println(sm.getRunningStrategies());
	}


}
