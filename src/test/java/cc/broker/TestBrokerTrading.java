package cc.broker;

import static org.junit.Assert.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import cc.beans.BrokerHandler;
import cc.model.Model.Decision;
import cc.monitors.StrategyMonitor;
import cc.stock.Stock;
import cc.strategy.Strategy;
import cc.strategy.Strategy.StrategyType;
import cc.strategy.Trade;

/**
 * The Class TestBrokerTrading.
 * 
 * @author marcom
 */
public class TestBrokerTrading {
	
	/** The t. */
	Trade t;
	
	/** The st. */
	Stock st;
	
	/** The sm. */
	StrategyMonitor sm;
	
	/** The bh. */
	BrokerHandler bh;


	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		List<Double> parameters = new LinkedList<Double>();
		parameters.add(20.0);
		parameters.add(99.4);
//		List<Integer> l = new ArrayList<Integer>();
//		l.add(1);
//		l.add(4);
//		List<Timestamp> time = new ArrayList<Timestamp>();
//		time.add(new Timestamp(1234_5678_9012_3456L));
//		time.add(new Timestamp(4321_5678_9012_3456L));
		bh = new BrokerHandler();
		try {
			t = new Trade("test_BABA_1",StrategyType.TwoMA,"BLUE", 20, 99.4, 
					Decision.BUY,  LocalDateTime.now(), "test");
			//sm = new StrategyMonitor();
			//sm.createStrategy("test", "BLUE", "TwoMA", parameters, 100.0, 0.05, 5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Test sent message is returned.
	 */
	@Test
	public void testSentMessageIsReturned() {
		String buyTag = "<buy>"+t.isBuy()+"</buy>";
		String idTag = "<id>"+222+"</id>";
		String priceTag = "<price>"+t.getPrice()+"</price>";
		String sizeTag = "<size>"+t.getAmount()+"</size>";
		String stockTag = "<stock>"+t.getStockName()+"</stock>";
		String dateTag = "<whenAsDate>"+t.getTimestamp()+"</whenAsDate>";
		bh.makeTrade(t);
		System.out.println("------------------ASDF--------------");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		String returnedXml = "";
		try {
			returnedXml = new String(Files.readAllBytes(Paths.get("BrokerResponseLog.txt")));
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("returned: "+returnedXml);
		assertTrue(returnedXml.contains(buyTag));
		assertTrue(returnedXml.contains(idTag));
		assertTrue(returnedXml.contains(priceTag));
		assertTrue(returnedXml.contains(stockTag));
		assertTrue(returnedXml.contains(dateTag.split("T")[0]));
		assertTrue(returnedXml.contains(sizeTag));
	}

}
