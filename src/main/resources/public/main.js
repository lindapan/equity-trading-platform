(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _graph_page_graph_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./graph-page/graph-page.component */ "./src/app/graph-page/graph-page.component.ts");
/* harmony import */ var _strategy_strategy_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./strategy/strategy.component */ "./src/app/strategy/strategy.component.ts");
/* harmony import */ var _running_strategy_running_strategy_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./running-strategy/running-strategy.component */ "./src/app/running-strategy/running-strategy.component.ts");
/* harmony import */ var _stock_detail_stock_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./stock-detail/stock-detail.component */ "./src/app/stock-detail/stock-detail.component.ts");
/* harmony import */ var _stocks_stocks_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./stocks/stocks.component */ "./src/app/stocks/stocks.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    { path: '', redirectTo: '/graphs', pathMatch: 'full' },
    { path: 'graphs', component: _graph_page_graph_page_component__WEBPACK_IMPORTED_MODULE_2__["GraphPageComponent"] },
    { path: 'runningStrategy', component: _running_strategy_running_strategy_component__WEBPACK_IMPORTED_MODULE_4__["RunningStrategyComponent"] },
    { path: 'stockDetail', component: _stock_detail_stock_detail_component__WEBPACK_IMPORTED_MODULE_5__["StockDetailComponent"] },
    { path: 'stocks', component: _stocks_stocks_component__WEBPACK_IMPORTED_MODULE_6__["StocksComponent"] },
    { path: 'strategy', component: _strategy_strategy_component__WEBPACK_IMPORTED_MODULE_3__["StrategyComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<app-navbar></app-navbar>\r\n<!-- <div style=\"text-align:center\">\r\n  <h1>\r\n    {{ title }}\r\n  </h1>\r\n\r\n</div> -->\r\n\r\n<!-- \r\n<div class=\"container\">\r\n\r\n    <div class=\"row\">\r\n        <app-stocks></app-stocks>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <app-strategy class=\"col-md-6 col-lg-6 col-sm-12\"></app-strategy>\r\n        <app-running-strategy class=\"col-md-6 col-lg-6 col-sm-12\"></app-running-strategy>\r\n    </div>\r\n\r\n</div> -->\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Equity Trading Platform';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _stocks_stocks_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./stocks/stocks.component */ "./src/app/stocks/stocks.component.ts");
/* harmony import */ var _stock_detail_stock_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./stock-detail/stock-detail.component */ "./src/app/stock-detail/stock-detail.component.ts");
/* harmony import */ var _shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/stock/stock.service */ "./src/app/shared/stock/stock.service.ts");
/* harmony import */ var _shared_strategy_strategy_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/strategy/strategy.service */ "./src/app/shared/strategy/strategy.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _strategy_strategy_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./strategy/strategy.component */ "./src/app/strategy/strategy.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _graph_page_graph_page_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./graph-page/graph-page.component */ "./src/app/graph-page/graph-page.component.ts");
/* harmony import */ var _running_strategy_running_strategy_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./running-strategy/running-strategy.component */ "./src/app/running-strategy/running-strategy.component.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_15__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _stocks_stocks_component__WEBPACK_IMPORTED_MODULE_4__["StocksComponent"],
                _stock_detail_stock_detail_component__WEBPACK_IMPORTED_MODULE_5__["StockDetailComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_12__["NavbarComponent"],
                _graph_page_graph_page_component__WEBPACK_IMPORTED_MODULE_13__["GraphPageComponent"],
                _strategy_strategy_component__WEBPACK_IMPORTED_MODULE_11__["StrategyComponent"],
                _running_strategy_running_strategy_component__WEBPACK_IMPORTED_MODULE_14__["RunningStrategyComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_9__["RouterModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_15__["ChartsModule"]
            ],
            providers: [_shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_6__["StockService"], _shared_strategy_strategy_service__WEBPACK_IMPORTED_MODULE_7__["StrategyService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/graph-page/graph-page.component.css":
/*!*****************************************************!*\
  !*** ./src/app/graph-page/graph-page.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/graph-page/graph-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/graph-page/graph-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container alert alert-info\" >\n    <div class=\"row\">\n        <div class=\"col-md\">\n\n            <h4 style=\"padding-top : 6px\">Add Stock : </h4>\n        </div>\n        <div class=\"col-md\">\n            <form>\n                <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" id=\"stockName\" name=\"stockName\" [(ngModel)]=\"stockName\" required>\n                <span class=\"input-group-btn\">\n                    <button type=\"submit\" class=\"btn btn-outline-secondary\" (click)=\"addStock()\" >Add</button>\n                </span>\n                </div>\n            </form>\n        </div>\n        <div style=\"padding-top : 6px\" class=\"col-md\" [hidden]=\"!stock || !valid\">\n            Stock Name: {{stock?.name}}\n        </div>\n        <div style=\"padding-top : 6px\" class=\"col-md\" [hidden]=\"!stock || !valid\">\n            Current Price: {{stock?.currentPrice}}\n        </div>\n\n    </div>\n</div>\n<div class=\"container alert alert-danger\" [hidden]=\"valid\">\n        <strong>\n             <p style=\"padding-top : 8px; text-align:center\">ERROR : Please Input a Valid Stock</p>\n        </strong>\n\n</div>\n<!-- <div class=\"container\">\n  <canvas id=\"myChart\"></canvas>\n</div> -->\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <div style=\"display: block;\">\n    <canvas baseChart width=\"400\" height=\"400\"\n                [datasets]=\"lineChartData\"\n                [labels]=\"lineChartLabels\"\n                [options]=\"lineChartOptions\"\n                [colors]=\"lineChartColors\"\n                [legend]=\"lineChartLegend\"\n                [chartType]=\"lineChartType\"\n                (chartHover)=\"chartHovered($event)\"\n                (chartClick)=\"chartClicked($event)\"></canvas>\n    </div>\n  </div>\n  <div class=\"col-md-6\" style=\"margin-bottom: 10px\">\n    <table class=\"table table-responsive table-condensed\">\n      <tr>\n        <th *ngFor=\"let label of lineChartLabels\">{{label}}</th>\n      </tr>\n      <tr *ngFor=\"let d of lineChartData\">\n        <td *ngFor=\"let label of lineChartLabels; let j=index\">{{d && d.data[j]}}</td>\n      </tr>\n    </table>\n    <button (click)=\"update()\">Update</button>\n    <button (click)=\"details()\">View Stock Details</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/graph-page/graph-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/graph-page/graph-page.component.ts ***!
  \****************************************************/
/*! exports provided: GraphPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GraphPageComponent", function() { return GraphPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/stock/stock.service */ "./src/app/shared/stock/stock.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import Rx from "rxjs/Rx";
var GraphPageComponent = /** @class */ (function () {
    function GraphPageComponent(stockService) {
        this.stockService = stockService;
        this.monitoredStocks = [];
        this.stockName = '';
        this.valid = true;
        // lineChart
        this.lineChartData = [
            { data: [65, 59, 80, 81, 56, 55, 40, 100, 43, 56], label: 'Series A' }
        ];
        this.lineChartLabels = ['0s', '15s', '30s', '45s', '60s', '75s', '90s', '105s', '120s', '135s', '150s', '165s', '180s', '195s', '210s', '225s', '240s', '255s', '270s', '285s', '300s', '315s', '330s', '345s', '360s', '375s', '390s', '405s', '420s', '435s', '450s', '465s', '480s', '495s', '510s', '525s', '540s', '555s', '570s', '585s', '600s', '615s', '630s', '645s', '660s', '675s', '690s', '705s', '720s', '735s', '750s', '765s', '780s', '795s', '810s', '825s', '840s', '855s', '870s', '885s', '900s', '915s', '930s', '945s', '960s', '975s', '990s', '1005s', '1020s', '1035s', '1050s', '1065s', '1080s', '1095s', '1110s', '1125s', '1140s', '1155s', '1170s', '1185s', '1200s', '1215s', '1230s', '1245s', '1260s', '1275s', '1290s', '1305s', '1320s', '1335s', '1350s', '1365s', '1380s', '1395s', '1410s', '1425s', '1440s', '1455s', '1470s', '1485s', '1500s', '1515s', '1530s', '1545s', '1560s', '1575s', '1590s', '1605s', '1620s', '1635s', '1650s', '1665s', '1680s', '1695s', '1710s', '1725s', '1740s', '1755s', '1770s', '1785s'];
        this.lineChartOptions = {
            responsive: true
        };
        this.lineChartColors = [
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(255, 99, 132, 1)',
                pointBackgroundColor: 'rgba(255, 99, 132, 1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            },
            {
                backgroundColor: 'rgba(77,83,96,0.2)',
                borderColor: 'rgba(31, 218, 34,1)',
                pointBackgroundColor: 'rgba(31, 218, 34,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(77,83,96,1)'
            },
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(148,159,177,1)',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            }
        ];
        this.lineChartLegend = true;
        this.lineChartType = 'line';
        //Observable.interval(10000).takeWhile(() => true).subscribe(() => this.function());
    }
    GraphPageComponent.prototype.function = function () {
        console.log("I am being called!");
    };
    GraphPageComponent.prototype.updateChart = function (days, prices, stock) {
        var _lineChartData = new Array(this.lineChartData.length);
        _lineChartData[0] = { data: new Array(prices.length), label: stock };
        for (var j = 0; j < prices.length; j++) {
            _lineChartData[0].data[j] = prices[j];
        }
        this.lineChartData = _lineChartData;
    };
    GraphPageComponent.prototype.randomize = function () {
        var _lineChartData = new Array(this.lineChartData.length);
        for (var i = 0; i < this.lineChartData.length; i++) {
            _lineChartData[i] = { data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label };
            for (var j = 0; j < this.lineChartData[i].data.length; j++) {
                _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
            }
        }
        this.lineChartData = _lineChartData;
    };
    GraphPageComponent.prototype.update = function () {
        for (var i = 0; i < this.monitoredStocks.length; i++) {
            this.updateData(this.monitoredStocks[i]);
        }
    };
    GraphPageComponent.prototype.addStock = function () {
        this.valid = true;
        console.log("input stock name: " + this.stockName);
        if (!this.stockName) {
            this.valid = false;
            return;
        }
        this.monitoredStocks.push(this.stockName);
        this.updateData(this.stockName);
        //this.pullNewMarketData();
    };
    GraphPageComponent.prototype.updateData = function (stockName) {
        var _this = this;
        this.stockService.getStock(stockName).subscribe(function (data) {
            var stock = data;
            var historical = JSON.parse("[" + data.historicalPrice + "]");
            var time = JSON.parse("[" + data.timePeriod + "]");
            _this.updateChart(time, historical, stock.name);
        });
    };
    GraphPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-graph-page',
            template: __webpack_require__(/*! ./graph-page.component.html */ "./src/app/graph-page/graph-page.component.html"),
            styles: [__webpack_require__(/*! ./graph-page.component.css */ "./src/app/graph-page/graph-page.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_1__["StockService"]])
    ], GraphPageComponent);
    return GraphPageComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.css":
/*!*********************************************!*\
  !*** ./src/app/navbar/navbar.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md navbar-dark bg-dark fixed-top\">\r\n  <a class=\"navbar-brand\" href=\"#\">DoubleD DOS</a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarsExampleDefault\" aria-controls=\"navbarsExampleDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item\">\r\n          <a routerLink=\"/graphs\" class=\"nav-link disabled\" href=\"#\">Graphs</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n          <a routerLink=\"/strategy\" class=\"nav-link disabled\" href=\"#\">Strategy</a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</nav>\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/running-strategy/running-strategy.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/running-strategy/running-strategy.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n    width: 100%;\r\n  }\r\n  \r\n  tr.example-detail-row {\r\n    height: 0;\r\n  }\r\n  \r\n  tr.example-element-row:not(.example-expanded-row):hover {\r\n    background: #f5f5f5;\r\n  }\r\n  \r\n  tr.example-element-row:not(.example-expanded-row):active {\r\n    background: #efefef;\r\n  }\r\n  \r\n  .example-element-row td {\r\n    border-bottom-width: 0;\r\n  }\r\n  \r\n  .example-element-detail {\r\n    overflow: hidden;\r\n    display: flex;\r\n  }\r\n  \r\n  .example-element-diagram {\r\n    min-width: 80px;\r\n    border: 2px solid black;\r\n    padding: 8px;\r\n    font-weight: lighter;\r\n    margin: 8px 0;\r\n    height: 104px;\r\n  }\r\n  \r\n  .example-element-symbol {\r\n    font-weight: bold;\r\n    font-size: 40px;\r\n    line-height: normal;\r\n  }\r\n  \r\n  .example-element-description {\r\n    padding: 16px;\r\n  }\r\n  \r\n  .example-element-description-attribution {\r\n    opacity: 0.5;\r\n  }"

/***/ }),

/***/ "./src/app/running-strategy/running-strategy.component.html":
/*!******************************************************************!*\
  !*** ./src/app/running-strategy/running-strategy.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<button type=\"submit\" class=\"btn btn-outline-secondary\" (click)=\"run()\" >Add</button>\r\n\r\n"

/***/ }),

/***/ "./src/app/running-strategy/running-strategy.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/running-strategy/running-strategy.component.ts ***!
  \****************************************************************/
/*! exports provided: RunningStrategyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunningStrategyComponent", function() { return RunningStrategyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/stock/stock.service */ "./src/app/shared/stock/stock.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];
var RunningStrategyComponent = /** @class */ (function () {
    function RunningStrategyComponent(stockService) {
        this.stockService = stockService;
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = ELEMENT_DATA;
    }
    // ngOnInit() {
    // }
    RunningStrategyComponent.prototype.run = function () {
        console.log("Running");
        this.stockService.startPricingService().subscribe(function (data) {
            console.log("Completed the call");
        });
    };
    RunningStrategyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-running-strategy',
            template: __webpack_require__(/*! ./running-strategy.component.html */ "./src/app/running-strategy/running-strategy.component.html"),
            styles: [__webpack_require__(/*! ./running-strategy.component.css */ "./src/app/running-strategy/running-strategy.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_1__["StockService"]])
    ], RunningStrategyComponent);
    return RunningStrategyComponent;
}());



/***/ }),

/***/ "./src/app/shared/stock/stock.service.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/stock/stock.service.ts ***!
  \***********************************************/
/*! exports provided: StockService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockService", function() { return StockService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StockService = /** @class */ (function () {
    function StockService(http) {
        this.http = http;
    }
    // getAll(): Observable<any> {
    //   return this.http.get('//localhost:8081/getStock');
    // }
    StockService.prototype.getStock = function (stockName) {
        return this.http.post('//localhost:8080/getStock', stockName);
    };
    StockService.prototype.startPricingService = function () {
        return this.http.post('//localhost:8080/startPricingService', "asdf");
    };
    StockService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StockService);
    return StockService;
}());



/***/ }),

/***/ "./src/app/shared/strategy/strategy.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/strategy/strategy.service.ts ***!
  \*****************************************************/
/*! exports provided: StrategyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StrategyService", function() { return StrategyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StrategyService = /** @class */ (function () {
    function StrategyService(http) {
        this.http = http;
    }
    StrategyService.prototype.addStrategy = function (list) {
        return this.http.post('//localhost:8080/addStrategy', list);
    };
    StrategyService.prototype.getRunningStrategy = function () {
        return this.http.get('//localhost:8080/getRunningStrategy');
    };
    StrategyService.prototype.getPausedStrategy = function () {
        return this.http.get('//localhost:8080/getPausedStrategy');
    };
    StrategyService.prototype.pauseStrategy = function (id) {
        return this.http.post('//localhost:8080/pauseStrategy', id);
    };
    StrategyService.prototype.stopStrategy = function (id) {
        return this.http.post('//localhost:8080/stopStrategy', id);
    };
    StrategyService.prototype.startStrategy = function (id) {
        return this.http.post('//localhost:8080/startStrategy', id);
    };
    StrategyService.prototype.getPerformance = function (id) {
        return this.http.post('//localhost:8080/getPerformance', id);
    };
    StrategyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StrategyService);
    return StrategyService;
}());



/***/ }),

/***/ "./src/app/stock-detail/stock-detail.component.css":
/*!*********************************************************!*\
  !*** ./src/app/stock-detail/stock-detail.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/stock-detail/stock-detail.component.html":
/*!**********************************************************!*\
  !*** ./src/app/stock-detail/stock-detail.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"stock\">\r\n\r\n  <h2>{{stock.name | uppercase}} Details</h2>\r\n  <div><span>name: </span>{{stock.name}}</div>\r\n  <div><span>high price: </span>{{stock.highPrice}}</div>\r\n   <div><span>low price: </span>{{stock.lowPrice}}</div>\r\n   <div><span>current price: </span>{{stock.currPrice}}</div>\r\n   <div><span>open: </span>{{stock.open}}</div>\r\n   <div><span>close: </span>{{stock.close}}</div>\r\n   <div><span>volumn: </span>{{stock.volume}}</div>\r\n\r\n</div>\r\n\r\n\r\n<div>this is stock details</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/stock-detail/stock-detail.component.ts":
/*!********************************************************!*\
  !*** ./src/app/stock-detail/stock-detail.component.ts ***!
  \********************************************************/
/*! exports provided: StockDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockDetailComponent", function() { return StockDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _stock__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../stock */ "./src/app/stock.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StockDetailComponent = /** @class */ (function () {
    function StockDetailComponent() {
    }
    StockDetailComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _stock__WEBPACK_IMPORTED_MODULE_1__["Stock"])
    ], StockDetailComponent.prototype, "stock", void 0);
    StockDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stock-detail',
            template: __webpack_require__(/*! ./stock-detail.component.html */ "./src/app/stock-detail/stock-detail.component.html"),
            styles: [__webpack_require__(/*! ./stock-detail.component.css */ "./src/app/stock-detail/stock-detail.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], StockDetailComponent);
    return StockDetailComponent;
}());



/***/ }),

/***/ "./src/app/stock.ts":
/*!**************************!*\
  !*** ./src/app/stock.ts ***!
  \**************************/
/*! exports provided: Stock */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stock", function() { return Stock; });
var Stock = /** @class */ (function () {
    function Stock() {
    }
    return Stock;
}());



/***/ }),

/***/ "./src/app/stocks/stocks.component.css":
/*!*********************************************!*\
  !*** ./src/app/stocks/stocks.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "selected {\r\n    background-color: #CFD8DC !important;\r\n    color: white;\r\n  }\r\n  .stocks {\r\n    margin: 0 0 2em 0;\r\n    list-style-type: none;\r\n    padding: 0;\r\n    width: 15em;\r\n    display: inline-block;\r\n  }\r\n  .stocks li {\r\n    display: inline-block;\r\n    cursor: pointer;\r\n    position: relative;\r\n    left: 0;\r\n    background-color: #EEE;\r\n    margin: .5em;\r\n    padding: .3em 0;\r\n    height: 1.6em;\r\n    border-radius: 4px;\r\n  }\r\n  .stocks li.selected:hover {\r\n    background-color: #BBD8DC !important;\r\n    color: white;\r\n  }\r\n  .stocks li:hover {\r\n    color: #607D8B;\r\n    background-color: #DDD;\r\n    left: .1em;\r\n  }\r\n  .stocks .text {\r\n    display: inline-block;\r\n    position: relative;\r\n    top: -3px;\r\n  }\r\n  .stocks .badge {\r\n    display: inline-block;\r\n    font-size: small;\r\n    color: white;\r\n    padding: 0.8em 0.7em 0 0.7em;\r\n    background-color: #607D8B;\r\n    line-height: 1em;\r\n    position: relative;\r\n    left: -1px;\r\n    top: -4px;\r\n    height: 1.8em;\r\n    margin-right: .8em;\r\n    border-radius: 4px 0 0 4px;\r\n  }\r\n  .flex-container {\r\n    display: flex;\r\n    height: 600px;\r\n    flex-wrap: wrap;\r\n    align-content: space-between;\r\n  }\r\n  .stock h4 { \r\n    display: inline-block;\r\n    padding-top : 5px;\r\n    font-size: 1em;\r\n    text-align: center;\r\n    font-weight: bold;\r\n}"

/***/ }),

/***/ "./src/app/stocks/stocks.component.html":
/*!**********************************************!*\
  !*** ./src/app/stocks/stocks.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container alert alert-info\" >\r\n    <div class=\"row\">\r\n        <div class=\"col-md\">\r\n\r\n            <h4 style=\"padding-top : 6px\">Get Stock Price : </h4>\r\n        </div>\r\n        <div class=\"col-md\">\r\n            <form>\r\n                <div class=\"input-group\">\r\n                <input type=\"text\" class=\"form-control\" id=\"stockName\" name=\"stockName\" [(ngModel)]=\"stockName\" required>\r\n                <span class=\"input-group-btn\">\r\n                    <button type=\"submit\" class=\"btn btn-outline-secondary\" (click)=\"getStock()\" >Search</button>\r\n                </span>\r\n                </div>\r\n            </form>\r\n        </div>\r\n        <div style=\"padding-top : 6px\" class=\"col-md\" [hidden]=\"!stock || !valid\">\r\n            Stock Name: {{stock?.name}}\r\n        </div>\r\n        <div style=\"padding-top : 6px\" class=\"col-md\" [hidden]=\"!stock || !valid\">\r\n            Current Price: {{stock?.price}}\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n<div class=\"container alert alert-danger\" [hidden]=\"valid\">\r\n        <strong>\r\n             <p style=\"padding-top : 8px; text-align:center\">ERROR : Please Input a Valid Stock</p>\r\n        </strong>\r\n\r\n     </div>\r\n"

/***/ }),

/***/ "./src/app/stocks/stocks.component.ts":
/*!********************************************!*\
  !*** ./src/app/stocks/stocks.component.ts ***!
  \********************************************/
/*! exports provided: StocksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StocksComponent", function() { return StocksComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/stock/stock.service */ "./src/app/shared/stock/stock.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StocksComponent = /** @class */ (function () {
    function StocksComponent(stockService, router) {
        this.stockService = stockService;
        this.router = router;
        this.valid = true;
        this.stockName = '';
    }
    StocksComponent.prototype.ngOnInit = function () {
        // this.stockService.getAll().subscribe(data => {
        //   this.stock = data;
        //   // console.log(this.stock);
        //   // console.log(this.stock.name);
        // });
    };
    StocksComponent.prototype.getStock = function () {
        var _this = this;
        this.valid = true;
        console.log("input stock name: " + this.stockName);
        if (!this.stockName) {
            this.valid = false;
            return;
        }
        this.stockService.getStock(this.stockName).subscribe(function (data) {
            _this.stock = data;
            console.log("get stock output" + _this.stock);
            console.log(_this.valid);
        });
    };
    StocksComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stocks',
            template: __webpack_require__(/*! ./stocks.component.html */ "./src/app/stocks/stocks.component.html"),
            styles: [__webpack_require__(/*! ./stocks.component.css */ "./src/app/stocks/stocks.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_stock_stock_service__WEBPACK_IMPORTED_MODULE_1__["StockService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], StocksComponent);
    return StocksComponent;
}());



/***/ }),

/***/ "./src/app/strategy/strategy.component.css":
/*!*************************************************!*\
  !*** ./src/app/strategy/strategy.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/strategy/strategy.component.html":
/*!**************************************************!*\
  !*** ./src/app/strategy/strategy.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<br>\r\n    <br>\r\n    <div class=\"container\">\r\n    <h3>Define Your Strategy</h3>\r\n    <br>\r\n\r\n    <div class=\"container alert alert-danger\" [hidden]=\"status || !trigger\">\r\n        <strong>\r\n             <p style=\"padding-top : 10px; text-align:center\">ERROR : Invalid Input</p>\r\n        </strong>\r\n     </div>\r\n\r\n     <div class=\"container alert alert-success\" [hidden]=\"!status || !trigger\">\r\n            <strong>\r\n                 <p style=\"padding-top : 10px; text-align:center\">SUCCESS</p>\r\n            </strong>\r\n         </div>\r\n    \r\n    <form>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-4 form-group\" >\r\n        <label for=\"stockName\">Strategy Name</label>\r\n        <input type=\"text\" class=\"form-control\" id=\"id\" name=\"id\" [(ngModel)]=\"id\" required>\r\n      </div>\r\n\r\n     \r\n        <div class=\"col-md-4 form-group\">\r\n            <label for=\"stockName\">Stock</label>\r\n            <input type=\"text\" class=\"form-control\" id=\"stockName\" name=\"stock\" [(ngModel)]=\"stock\" required>\r\n        </div>\r\n    \r\n        <div class=\"col-md-4 form-group\">\r\n            <label for=\"strategies\">Strategy</label>\r\n            <select class=\"form-control\" id=\"strategy\" name=\"strategy\" [(ngModel)]=\"strategy\" required>\r\n                <option *ngFor=\"let strategy of strategies\">{{strategy}}</option>\r\n            </select>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n      <div class=\"form-group col-md-4\">\r\n          <label for=\"stopping\">Stopping Threshold</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"stopping\" name=\"stopping\" [(ngModel)]=\"stopping\">\r\n      </div>\r\n\r\n      <div class=\"form-group col-md-4\">\r\n          <label for=\"exit\">Exit Threshold</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"exit\" name=\"exit\" [(ngModel)]=\"exit\">\r\n      </div>\r\n      <div class=\"form-group col-md-4\">\r\n          <label for=\"amount\">Amount</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"amount\" name=\"amount\" [(ngModel)]=\"amount\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n      <div class=\"form-group col-md-6\" [hidden]=\"strategy!='TwoMA'\">\r\n          <label for=\"param2\">Long Term Window</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"MA1\" name=\"longMA\" [(ngModel)]=\"longMA\">\r\n      </div>\r\n\r\n      <div class=\"form-group col-md-6\" [hidden]=\"strategy!='TwoMA'\">\r\n          <label for=\"param1\">Short Term Window</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"param1\" name=\"shortMA\" [(ngModel)]=\"shortMA\">\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"form-group col-md-6\" [hidden]=\"strategy!='BollingerBands'\">\r\n          <label for=\"param2\">Window Size</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"param2\" name=\"windowBB\" [(ngModel)]=\"windowBB\">\r\n      </div>\r\n\r\n      <div class=\"form-group col-md-6\" [hidden]=\"strategy!='BollingerBands'\">\r\n          <label for=\"param2\">Deviation</label>\r\n          <input type=\"text\" class=\"form-control\" id=\"param2\" name=\"deviationBB\" [(ngModel)]=\"deviationBB\">\r\n      </div>\r\n    </div>\r\n      <button type=\"submit\" class=\"btn btn-outline-secondary\" (click)=\"addStrategy(); getRunningStrategy()\">Start</button>\r\n    </form>\r\n</div>\r\n\r\n    <br>\r\n    <br>\r\n\r\n    <div class=\"container alert alert-info\" [hidden]=\"!pnl\">\r\n        \r\n            <p style=\"padding-top : 8px; text-align:center\">Strategy Name:{{currentid}}  PnL:{{pnl}} </p>\r\n       \r\n\r\n    </div>\r\n<div class=\"container\">\r\n\r\n<button type=\"button\" class=\"btn btn-dark btn-lg\" (click)=\"running=true; getRunningStrategy(); getPerformance()\">Running Strategy</button>\r\n<button type=\"button\" class=\"btn btn-light btn-lg\" (click)=\"running=false; getPausedStrategy()\">Paused Strategy</button>\r\n<!-- {{run | json}} -->\r\n\r\n        \r\n<table class=\"table\" [hidden]=\"!running\" >\r\n        <thead class=\"thead-dark\">\r\n          <tr >\r\n            <th scope=\"col\" *ngFor=\"let col of runningcolumns\" >{{col}}</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let a of run\">\r\n            <th scope=\"row\">{{a.id}}</th>\r\n            <td>{{a.stockName}}</td>\r\n            <td>{{a.type}}</td>\r\n            <td>{{a.stoppingThreshold}}</td>\r\n            <td>{{a.exitThreshold}}</td>\r\n            <td>{{a.amount}}</td>\r\n            <td>{{a.param1}}</td>\r\n            <td>{{a.param2}}</td>\r\n            <td><button type=\"button\" class=\"btn btn-warning\" (click) = \"pauseStrategy(a.id)\">Pause</button></td>\r\n            <td><button type=\"button\" class=\"btn btn-danger\" (click) = \"stopStrategy(a.id)\">Remove</button></td>\r\n            <td><button type=\"button\" class=\"btn btn-info\" (click) = \"getPerformance(a.id)\">Performance</button></td>\r\n          </tr>\r\n        </tbody>\r\n</table>\r\n\r\n<table class=\"table\" [hidden]=\"running\">\r\n        <thead class=\"thead-light\">\r\n            <tr>\r\n                <th scope=\"col\" *ngFor=\"let col of pausecolumns\" >{{col}}</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let a of pause\">\r\n                <th scope=\"row\">{{a.id}}</th>\r\n                <td>{{a.stockName}}</td>\r\n                <td>{{a.type}}</td>\r\n                <td>{{a.stoppingThreshold}}</td>\r\n                <td>{{a.exitThreshold}}</td>\r\n                <td>{{a.amount}}</td>\r\n                <td>{{a.param1}}</td>\r\n                <td>{{a.param2}}</td>\r\n                <td><button type=\"button\" class=\"btn btn-success\" (click) = \"startStrategy(a.id)\">Start</button></td>\r\n            </tr>\r\n        </tbody>\r\n      </table>\r\n      \r\n    </div>\r\n"

/***/ }),

/***/ "./src/app/strategy/strategy.component.ts":
/*!************************************************!*\
  !*** ./src/app/strategy/strategy.component.ts ***!
  \************************************************/
/*! exports provided: StrategyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StrategyComponent", function() { return StrategyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_strategy_strategy_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared/strategy/strategy.service */ "./src/app/shared/strategy/strategy.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StrategyComponent = /** @class */ (function () {
    function StrategyComponent(strategyService, router) {
        this.strategyService = strategyService;
        this.router = router;
        this.strategy = "";
        this.status = true;
        this.trigger = false;
        this.running = true;
        this.runningcolumns = ["id", "stockName", "type", "stoppingThreshold", "exitThreshold", "amount", "param1", "param2", "Pause", "Remove", "Performance"];
        this.pausecolumns = ["id", "stockName", "type", "stoppingThreshold", "exitThreshold", "amount", "param1", "param2", "Start"];
        this.strategies = ['', 'TwoMA', 'BollingerBands'];
        this.submitted = false;
    }
    StrategyComponent.prototype.ngOnInit = function () {
        this.getRunningStrategy();
    };
    StrategyComponent.prototype.onSubmit = function () { this.submitted = true; };
    StrategyComponent.prototype.addStrategy = function () {
        var _this = this;
        this.trigger = true;
        if (this.strategy == 'TwoMA') {
            this.list = [this.id, this.strategy, this.stock, this.stopping, this.exit, this.amount, this.longMA, this.shortMA];
            console.log(this.list);
        }
        else if (this.strategy == "BollingerBands") {
            this.list = [this.id, this.strategy, this.stock, this.stopping, this.exit, this.amount, this.windowBB, this.deviationBB];
            console.log(this.list);
        }
        this.strategyService.addStrategy(this.list).subscribe(function (data) {
            _this.response = data.status;
            console.log(_this.response);
            if (_this.response == "success") {
                _this.status = true;
                console.log("success!!");
            }
            else if (_this.response == "failed") {
                _this.status = false;
                console.log("failed!!");
            }
        });
        this.id = "";
        this.stock = "";
        this.strategy = "";
        this.stopping = "";
        this.exit = "";
        this.amount = "";
        this.longMA = "";
        this.shortMA = "";
        this.windowBB = "";
        this.deviationBB = "";
    };
    StrategyComponent.prototype.getRunningStrategy = function () {
        var _this = this;
        console.log("running strategy");
        this.strategyService.getRunningStrategy().subscribe(function (data) {
            console.log(data);
            _this.run = data;
        });
    };
    StrategyComponent.prototype.getPausedStrategy = function () {
        var _this = this;
        console.log("paused strategy");
        this.strategyService.getPausedStrategy().subscribe(function (data) {
            console.log(data);
            _this.pause = data;
        });
    };
    StrategyComponent.prototype.pauseStrategy = function (id) {
        var _this = this;
        console.log(id);
        this.strategyService.pauseStrategy(id).subscribe(function (data) {
            // this.pause = data;
            _this.getRunningStrategy();
            _this.getPausedStrategy();
        });
    };
    StrategyComponent.prototype.stopStrategy = function (id) {
        var _this = this;
        console.log(id);
        this.strategyService.stopStrategy(id).subscribe(function (data) {
            // this.pause = data;
            _this.getRunningStrategy();
            _this.getPausedStrategy();
        });
    };
    StrategyComponent.prototype.startStrategy = function (id) {
        var _this = this;
        console.log(id);
        this.strategyService.startStrategy(id).subscribe(function (data) {
            // this.pause = data;
            _this.getRunningStrategy();
            _this.getPausedStrategy();
        });
    };
    StrategyComponent.prototype.getPerformance = function (id) {
        var _this = this;
        console.log(id);
        this.strategyService.getPerformance(id).subscribe(function (data) {
            console.log(data);
            _this.currentid = id;
            _this.pnl = data.pnl;
            // this.roi = data.roi;
            // this.getRunningStrategy();
            // this.getPausedStrategy();
        });
    };
    StrategyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-strategy',
            template: __webpack_require__(/*! ./strategy.component.html */ "./src/app/strategy/strategy.component.html"),
            styles: [__webpack_require__(/*! ./strategy.component.css */ "./src/app/strategy/strategy.component.css")]
        }),
        __metadata("design:paramtypes", [_shared_strategy_strategy_service__WEBPACK_IMPORTED_MODULE_1__["StrategyService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], StrategyComponent);
    return StrategyComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\marcom\Documents\equity-trading-client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map