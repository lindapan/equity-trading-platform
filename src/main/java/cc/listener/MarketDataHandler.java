package cc.listener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Set;
import cc.stock.Stock;
import cc.strategy.Strategy;

/**
 * The Class MarketDataHandler.
 * 
 * author: Jack Guo
 */
public class MarketDataHandler extends Observable implements Runnable{
	
	/** The Constant instance. */
	private static final MarketDataHandler instance = new MarketDataHandler();
	
	/** The period. */
	private static int period = 120;
	
	/** The list of stocks. */
	private static Set<String> listOfStocks;;
	
	/** The cache. */
	private static HashMap<String, Stock> cache;
	
	/** The subscribers. */
	private static HashMap<String, LinkedList<Strategy>> subscribers;

	/**
	 * Instantiates a new market data handler.
	 */
	private MarketDataHandler() {
		listOfStocks = new HashSet<String>();
		cache = new HashMap<String, Stock>();
		subscribers = new HashMap<String, LinkedList<Strategy>>();
	}

	/**
	 * Gets the single instance of MarketDataHandler.
	 *
	 * @return single instance of MarketDataHandler
	 */
	public static MarketDataHandler getInstance() {
		return instance;
	}

	/**
	 * Gets the stock info in range.
	 *
	 * @param stockName the stock name
	 * @param period the period
	 * @return the stock info in range
	 * @throws Exception the exception
	 */
	private static Stock getStockInfoInRange(String stockName, int period) throws Exception {
		LinkedList<Double> highPrices = new LinkedList<Double>();
		LinkedList<Double> lowPrices = new LinkedList<Double>();
		LinkedList<Double> open = new LinkedList<Double>();
		LinkedList<Double> close = new LinkedList<Double>();
		LinkedList<Integer> volumes = new LinkedList<Integer>();
		LinkedList<LocalDateTime> timestamps = new LinkedList<LocalDateTime>();
		Stock queryStock = null;

		StringBuilder result = new StringBuilder();
		String pURL = "http://incanada1.conygre.com:9080/prices/" + stockName + "?periods=" + period;
		URL url = new URL(pURL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		
		rd.readLine();
		while ((line = rd.readLine()) != null) {
			result.append(line);
			String[] priceLine = line.split(",");
			try {
				LocalDateTime timestamp = LocalDateTime.parse(priceLine[0], dateFormat);
				timestamps.add(timestamp);
				open.add(Double.parseDouble(priceLine[1]));
				highPrices.add(Double.parseDouble(priceLine[2]));
				lowPrices.add(Double.parseDouble(priceLine[3]));
				close.add(Double.parseDouble(priceLine[4]));
				volumes.add(Integer.parseInt(priceLine[5]));
			} catch (Exception e) {
				e.printStackTrace();
			}

//			for (String s : priceLine) {
//				System.out.print(s + ",");
//			}
//			System.out.println("");
		}
		
		rd.close();
		queryStock = new Stock(stockName, highPrices, lowPrices, open, close, volumes, timestamps);
		return queryStock;
	}

	/**
	 * Gets the stock from API.
	 *
	 * @param stockName the stock name
	 * @param p the p
	 * @return the stock from API
	 * @throws Exception the exception
	 */
	public static Stock getStockFromAPI(String stockName, int p) throws Exception {
		return getStockInfoInRange(stockName, p);
	}

	/**
	 * Gets the stock from API.
	 *
	 * @param stockName the stock name
	 * @return the stock from API
	 * @throws Exception the exception
	 */
	public static Stock getStockFromAPI(String stockName) throws Exception {
		return getStockInfoInRange(stockName, period);
	}

	/**
	 * Update list of stocks.
	 */
	private static void updateListOfStocks() {
		listOfStocks = subscribers.keySet();

	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		boolean run = true;
		while (run) {
			System.out.println("One iteration");
			updateListOfStocks();
			for (String s : listOfStocks) {
//				System.out.println(getStockFromAPI(s).getClose().size());
				try {
					cache.put(s, getStockFromAPI(s));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			publish();
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			run = false;
		}
		
	}

	/**
	 * Removes the stock.
	 *
	 * @param s the s
	 */
	public static void removeStock(String s) {
		try {
			listOfStocks.remove(s);
		} catch (Exception e) {

		}
	}

	/**
	 * Subscribe.
	 *
	 * @param stockName the stock name
	 * @param strategy the strategy
	 */
	public static void subscribe(String stockName, Strategy strategy) {
		if (subscribers.containsKey(stockName)) {
			subscribers.get(stockName).add(strategy);
		} else {
			LinkedList<Strategy> newList = new LinkedList<Strategy>();
			newList.add(strategy);
			subscribers.put(stockName, newList);
		}
	}

	/**
	 * Unsubscribe.
	 *
	 * @param stockName the stock name
	 * @param strategy the strategy
	 */
	public static void unsubscribe(String stockName, Strategy strategy) {
		if (subscribers.containsKey(stockName)) {
			LinkedList<Strategy> list = subscribers.get(stockName);
			list.remove(strategy);
			if (list.size() == 0) {
				subscribers.remove(stockName);
			} else {
				subscribers.put(stockName, list);
			}
		}
	}

	/**
	 * Publish.
	 */
	public synchronized static void publish() {
		for (String stockName : subscribers.keySet()) {
			LinkedList<Strategy> listOfStrategy = subscribers.get(stockName);
			for (Strategy s : listOfStrategy) {
				try {
					System.out.println("Updating to "+s.getId());
					s.updateStock(cache.get(stockName));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Clear subscribers.
	 */
	public static void clearSubscribers() {
		subscribers.clear();
	}
	
	/**
	 * Gets the subscribers.
	 *
	 * @return the subscribers
	 */
	//For testing, remove later
	public static HashMap<String, LinkedList<Strategy>> getSubscribers() {
		return subscribers;
	}
//	public static void main(String[] args) throws Exception {
//		ExecutorService executor = Executors.newFixedThreadPool(10);
//		executor.execute(run());
//		executor.execute(System.out.println("Hello"));
//	}
}
