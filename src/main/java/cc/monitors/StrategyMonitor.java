package cc.monitors;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import cc.services.DatabaseMethods;
import cc.strategy.Strategy;
import cc.strategy.Strategy.Status;

// TODO: Auto-generated Javadoc
/**
 * The Class StrategyMonitor.
 * 
 * @author Jack Guo, marcom
 */

public class StrategyMonitor {

	/** The Constant LOGGER. */
	private final static Logger LOGGER = Logger.getLogger(StrategyMonitor.class.getName());

	/** The fh. */
	private FileHandler fh;

	/** The strategies. */
	private static List<Strategy> strategies;

	/** The running strategies. */
	private static List<Strategy> runningStrategies;

	/** The paused strategies. */
	private static List<Strategy> pausedStrategies;

	/** The dm. */
	private DatabaseMethods dm;

	/** The Constant instance. */
	private static final StrategyMonitor instance = new StrategyMonitor();

	/**
	 * Gets the single instance of StrategyMonitor.
	 *
	 * @return single instance of StrategyMonitor
	 */
	public static StrategyMonitor getInstance() {
		return instance;
	}

	/**
	 * Instantiates a new strategy monitor.
	 */
	private StrategyMonitor() {

		strategies = new LinkedList<Strategy>();

		runningStrategies = new LinkedList<Strategy>();
		for (Strategy s : strategies) {
			if (s.getStatus().equals(Status.RUNNING)) {
				runningStrategies.add(s);
			}
		}

		pausedStrategies = new LinkedList<Strategy>();
		for (Strategy s : strategies) {
			if (s.getStatus().equals(Status.STOPPED)) {
				pausedStrategies.add(s);
			}
		}
		try {
			fh = new FileHandler("StrategyMonitor.log");
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LOGGER.setUseParentHandlers(false);
		LOGGER.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();

		fh.setFormatter(formatter);
		dm = DatabaseMethods.getInstance();
		try {
			loadFromDatabase();
			LOGGER.info(strategies.size() + " strategies loaded from database.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.SEVERE, "Exception occur", e);
			e.printStackTrace();
		}
	}

	/**
	 * Load from database.
	 *
	 * @throws Exception the exception
	 */
	public void loadFromDatabase() throws Exception {
		List<Strategy> persistedStrategies = getStrategiesFromDB();
		if (persistedStrategies != null) {
			for (Strategy s : persistedStrategies) {
				Strategy parsedStrategy = new Strategy(s);
				strategies.add(parsedStrategy);

//				s.persistTrades();
//				s.initVars();
//				System.out.println(s.getId()+s.getType().toString());
//				s.setModel(s.getType().toString());
				if (s.getStatus().equals(Status.RUNNING)) {
					parsedStrategy.subscribeStrategy();
					runningStrategies.add(parsedStrategy);
				} else {
					pausedStrategies.add(parsedStrategy);
				}
			}
		}
	}

	/**
	 * Gets the list of stocks.
	 *
	 * @return the list of stocks
	 */
	public Set<String> getListOfStocks() {
		Set<String> stocks = new HashSet<String>();
		for (Strategy s : strategies) {
			stocks.add(s.getStockName());
		}
		return stocks;
	}

	/**
	 * Creates the strategy.
	 *
	 * @param id                 the id
	 * @param strategyTypeString the strategy type string
	 * @param stockName          the stock name
	 * @param stratParameters    the strat parameters
	 * @param stoppingThreshold  the stopping threshold
	 * @param exitThreshold      the exit threshold
	 * @param amount             the amount
	 * @throws Exception the exception
	 */
	public void createStrategy(String id, String strategyTypeString, String stockName, List<Double> stratParameters,
			double stoppingThreshold, double exitThreshold, int amount) throws Exception {

		try {
			Strategy newStrategy = new Strategy(id, strategyTypeString, stockName, stratParameters, stoppingThreshold,
					exitThreshold, amount);
			System.out.println(
					"****************************From StrategyMonitor --- createStrategy *******************************");
			System.out.println(newStrategy.getId());
			System.out.println(newStrategy.getType());
			System.out.println(newStrategy.getStockName());
			System.out.println(newStrategy.getStrategyParameters());
			System.out.println(newStrategy.getThreshold());
			System.out.println(newStrategy.getAmount());

			strategies.add(newStrategy);
			runningStrategies.add(newStrategy);
			dm.writeToDatabase(newStrategy);
			LOGGER.info("Strategy " + id + " created and written to DB.");

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
			throw new Exception();
		}
	}

	/**
	 * Creates the strategy.
	 *
	 * @param id                 the id
	 * @param strategyTypeString the strategy type string
	 * @param stockName          the stock name
	 * @param stoppingThreshold  the stopping threshold
	 * @param exitThreshold      the exit threshold
	 * @param amount             the amount
	 * @param params1            the params 1
	 * @param params2            the params 2
	 * @return the string
	 */
	public String createStrategy(String id, String strategyTypeString, String stockName, String stoppingThreshold,
			String exitThreshold, String amount, String params1, String params2) {

		List<Double> params = new LinkedList<Double>();
		double param1 = 0.0;
		double param2 = 0.0;
		double stopThresh;
		double exitThresh;
		int amt;
		String res = "";
		try {
			param1 = Double.parseDouble(params1);
			param2 = Double.parseDouble(params2);
			params.add(param1);
			params.add(param2);
			stopThresh = Double.parseDouble(stoppingThreshold);
			exitThresh = Double.parseDouble(exitThreshold);
			amt = Integer.parseInt(amount);

			System.out.println(
					"****************************From StrategyMonitor --- createStrategy All String *******************************");
			System.out.println(param1);
			System.out.println(param2);
			System.out.println(stopThresh);
			System.out.println(exitThresh);
			System.out.println(amt);
			createStrategy(id, strategyTypeString, stockName, params, stopThresh, exitThresh, amt);
			res = "success";
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
			return "failed";
//			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Removes the strategy.
	 *
	 * @param id the id
	 * @throws Exception the exception
	 */
	public void removeStrategy(String id) throws Exception {
		try {
			Strategy foundStrategy = getStrategy(id);
			strategies.remove(foundStrategy);
			foundStrategy.unsubscribeStrategy();
			dm.removeFromDatabase(foundStrategy);
			LOGGER.info("Strategy " + id + " removed.");
			if (foundStrategy.getStatus().equals(Status.RUNNING)) {
				runningStrategies.remove(foundStrategy);
			}

			if (foundStrategy.getStatus().equals(Status.STOPPED)) {
				runningStrategies.remove(foundStrategy);
			}

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
			throw new Exception("No such strategy in monitor");
		}
	}

	/**
	 * Removes the all strategy.
	 *
	 * @throws Exception the exception
	 */
	public void removeAllStrategy() throws Exception {
		for (Strategy s : strategies) {
			s.pause();
			dm.mergeToDatabase(s);
			LOGGER.info("All strategies removed.");
		}
		strategies.clear();
		runningStrategies.clear();
		pausedStrategies.clear();

	}

	/**
	 * Gets the all strategies.
	 *
	 * @return the all strategies
	 */
	public List<Strategy> getAllStrategies() {
		return strategies;
	}

	/**
	 * Gets the running strategies.
	 *
	 * @return the running strategies
	 */
	public List<Strategy> getRunningStrategies() {

		return runningStrategies;
	}

	/**
	 * Gets the paused strategies.
	 *
	 * @return the paused strategies
	 */
	public List<Strategy> getPausedStrategies() {

		return pausedStrategies;
	}

	/**
	 * Start strategy.
	 *
	 * @param id the id
	 * @throws Exception the exception
	 */
	public void startStrategy(String id) throws Exception {
		Strategy s = null;
		System.out.println(getStrategy(id));
		if ((s = getStrategy(id)) != null && s.getStatus().equals(Status.STOPPED)) {
			runningStrategies.add(s);
			pausedStrategies.remove(s);
			s.start();
			dm.mergeToDatabase(s);
			LOGGER.info("Strategy " + id + " started.");
		}
	}

	/**
	 * Pause strategy.
	 *
	 * @param id the id
	 * @throws Exception the exception
	 */
	public void pauseStrategy(String id) throws Exception {
		Strategy s = null;
		if ((s = getStrategy(id)) != null && s.getStatus().equals(Status.RUNNING)) {
			runningStrategies.remove(s);
			pausedStrategies.add(s);
			s.pause();
			dm.mergeToDatabase(s);
			LOGGER.info("Strategy " + id + " paused.");
		}
	}

	/**
	 * Gets the strategy.
	 *
	 * @param id the id
	 * @return the strategy
	 */
	public Strategy getStrategy(String id) {
		Strategy foundStrategy = null;
		for (Strategy s : strategies) {
			if (s.getId().equals(id)) {
				foundStrategy = s;
			}
		}
		return foundStrategy;
	}

	/**
	 * Stop all strategy.
	 *
	 * @throws Exception the exception
	 */
	public void stopAllStrategy() throws Exception {
		for (Strategy s : strategies) {
			dm.mergeToDatabase(s);
			for (Strategy s1 : runningStrategies) {
				pausedStrategies.add(s1);
			}
			s.pause();
		}
		LOGGER.info("All strategies paused.");
		runningStrategies.clear();
	}

	/**
	 * Start all strategy.
	 *
	 * @throws Exception the exception
	 */
	public void startAllStrategy() throws Exception {
		for (Strategy s : strategies) {

			dm.mergeToDatabase(s);
			for (Strategy s1 : pausedStrategies) {
				runningStrategies.add(s1);
			}
			s.start();

		}
		LOGGER.info("All strategies started.");
		pausedStrategies.clear();
	}

	/**
	 * Gets the strategies from DB.
	 *
	 * @return the strategies from DB
	 */
	@SuppressWarnings("unchecked")
	public List<Strategy> getStrategiesFromDB() {
		List<Strategy> res = null;
		String query = "SELECT s FROM Strategy s";
		try {
			res = dm.executeQuery(query, Strategy.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Gets the running strategies from DB.
	 *
	 * @return the running strategies from DB
	 */
	@SuppressWarnings("unchecked")
	public List<Strategy> getRunningStrategiesFromDB() {
		List<Strategy> res = null;
		String query = "SELECT s FROM Strategy s WHERE s.Status = 'RUNNING'";
		try {
			res = dm.executeQuery(query, Strategy.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Gets the paused strategies from DB.
	 *
	 * @return the paused strategies from DB
	 */
	@SuppressWarnings("unchecked")
	public List<Strategy> getPausedStrategiesFromDB() {
		List<Strategy> res = null;
		String query = "SELECT s FROM Strategy s WHERE s.Status = 'STOPPED'";
		try {
			res = dm.executeQuery(query, Strategy.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Gets the strategy from DB.
	 *
	 * @param strategyId the strategy id
	 * @return the strategy from DB
	 */
	public Strategy getStrategyFromDB(String strategyId) {
		Strategy res = null;
		try {
			res = (Strategy) dm.find(Strategy.class, strategyId);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public String getProfitOrLossFromStrategy(String id) {
		String res = "0.0";
		Strategy foundStrategy = getStrategyFromDB(id);
		System.out.println(foundStrategy + " "+ id);
		List<Double> hist;
		try {
			hist = foundStrategy.getProfitOrLossHistory();
			if (hist != null) {
				for(int i=hist.size()-1; i>=0; i--) {
					if(hist.get(i) != 0) {
						Double currProfitOrloss = hist.get(i);
						res = Double.toString((currProfitOrloss));
						break;
					}	
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;
	}

	public String getROIFromStrategy(String id) {
		String res = "0.0";
		Strategy foundStrategy = getStrategyFromDB(id);
		List<Double> hist;
		try {
			hist = foundStrategy.getROIHistory();
			if (hist != null) {
				for(int i=hist.size()-1; i>=0; i--) {
					if(hist.get(i) != 0) {
						Double currROI = hist.get(i);
						res = Double.toString((currROI));
						break;
					}	
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;
	}

}
