package cc.monitors;

import java.util.HashMap;
import java.util.List;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cc.listener.MarketDataHandler;
import cc.stock.Stock;

/**
 * The Class StockMonitor.
 * 
 * author: Jack Guo, Marco Martin
 */
public class StockMonitor {
	
	/** The Constant instance. */
	private static final StockMonitor instance = new StockMonitor();
	
	/** The stocks. */
	private HashMap<String, Stock> stocks;
	
	/**
	 * Gets the single instance of StockMonitor.
	 *
	 * @return single instance of StockMonitor
	 */
	public static StockMonitor getInstance() {
		return instance;
	}
	
	/**
	 * Instantiates a new stock monitor.
	 */
	private StockMonitor() {
		stocks = new HashMap<String, Stock>();
	}

	/**
	 * Gets the stock.
	 *
	 * @param stockName the stock name
	 * @return the stock
	 */
	public Stock getStock(String stockName) {
		Stock target = null;
		try {
			target = stocks.get(stockName);
		} catch (Exception e) {
			target = null;
		}
		return target;
	}

//	@SuppressWarnings("unchecked")
//	public JSONObject getAllStocks() {
//		JSONObject json = new JSONObject();
//		((Map<String, Stock>) json).putAll( stocks );
//		return json;
//	}
	
	/**
 * Gets the stock as json.
 *
 * @param stockName the stock name
 * @return the stock as json
 */
@SuppressWarnings("unchecked")
	public JsonObject getStockAsJson(String stockName) {
		
		JsonObject json = new JsonObject();
		Stock newStock = null;
		try {
			newStock = MarketDataHandler.getStockFromAPI(stockName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JsonArray seconds = new JsonArray();
		List<Double> prices = newStock.getClose(); 
		for(int i = 0; i<prices.size(); i++) {
			seconds.add(i*15);
		}
		
		JsonArray allPrices = new JsonArray();
		for(int i = 0; i<prices.size(); i++) {
			allPrices.add(prices.get(i));
		}
		
		json.addProperty("name", newStock.getListing());
		json.add("historicalPrice", allPrices);
		json.add("timePeriod", seconds);
		json.addProperty("currentPrice", prices.get(prices.size()-1));
		return json;
	}

	/**
	 * Adds the stock.
	 *
	 * @param stockName the stock name
	 * @param period the period
	 */
	// This method should connect to front end
	public void addStock(String stockName, int period) {
		Stock newStock;
		try {
			newStock = MarketDataHandler.getStockFromAPI(stockName, period);
			stocks.put(newStock.getListing(), newStock);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds the stock.
	 *
	 * @param stockName the stock name
	 */
	public void addStock(String stockName) {
		Stock newStock;
		try {
			newStock = MarketDataHandler.getStockFromAPI(stockName);
			stocks.put(newStock.getListing(), newStock);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes the stock.
	 *
	 * @param stockName the stock name
	 */
	public void removeStock(String stockName) {
		try {
			stocks.remove(stockName);
		} catch (Exception e) {
			System.out.println("No stock to be removed");
		}
	}

	/**
	 * Gets the number of stocks.
	 *
	 * @return the number of stocks
	 */
	public int getNumberOfStocks() {
		return stocks.size();
	}

	/**
	 * Removes the all stocks.
	 */
	public void removeAllStocks() {
		stocks.clear();
	}
}
