package cc.services;

import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import cc.strategy.Strategy;

/**
 * The Class DatabaseMethods.
 * 
 * author: Jack Guo
 */
public class DatabaseMethods {
	
	/** The factory. */
	private static EntityManagerFactory factory;
	
	/** The em. */
	private static EntityManager em;
	
	/** The Constant instance. */
	private static final DatabaseMethods instance = new DatabaseMethods();

	/**
	 * Instantiates a new database methods.
	 */
	private DatabaseMethods() {
		factory = Persistence.createEntityManagerFactory("oracle");
		em = factory.createEntityManager();

	}

	/**
	 * Gets the single instance of DatabaseMethods.
	 *
	 * @return single instance of DatabaseMethods
	 */
	public static DatabaseMethods getInstance() {
		return instance;
	}

	/**
	 * Merge to database.
	 *
	 * @param s the s
	 * @throws Exception the exception
	 */
	public void mergeToDatabase(Object s) throws Exception {
		try {
			em.getTransaction().begin();
			em.merge(s);
			em.getTransaction().commit();
		} catch (Exception e) {
			throw new Exception();
//			e.printStackTrace();
		}
	}

	/**
	 * Write to database.
	 *
	 * @param s the s
	 * @throws Exception the exception
	 */
	public void writeToDatabase(Object s) throws Exception {
		try {
			em.getTransaction().begin();
			em.persist(s);
			em.getTransaction().commit();
		} catch (Exception e) {
			throw new Exception();
//			e.printStackTrace();
		}
	}

	/**
	 * Removes the from database.
	 *
	 * @param s the s
	 * @throws Exception the exception
	 */
	public void removeFromDatabase(Object s) throws Exception {
		try {
			em.getTransaction().begin();
			em.remove(s);
			em.getTransaction().commit();
		} catch (Exception e) {
			throw new Exception();
//			e.printStackTrace();
		}
	}
	

	/**
	 * Execute query.
	 *
	 * @param query the query
	 * @param variable the variable
	 * @param c the c
	 * @return the list
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List executeQuery(String query, Object variable, Class c) throws Exception {
		List result = null;
		try {
			Query q = em.createQuery(query, c);
			q.setParameter("placeholder", variable);
			result = q.getResultList();
			System.out.println(result);
		} catch (Exception e) {
//			throw new Exception();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Execute query.
	 *
	 * @param query the query
	 * @param c the c
	 * @return the list
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List executeQuery(String query, Class c) throws Exception {
		List result = null;
		try {
			Query q = em.createQuery(query, c);
			result = q.getResultList();
			System.out.println(result);
		} catch (Exception e) {
//			throw new Exception();
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Find.
	 *
	 * @param c the c
	 * @param id the id
	 * @return the object
	 * @throws Exception the exception
	 */
	public Object find(Class c, String id) throws Exception{
		Object result = null;
		try {
			result = em.find(c, id);
		} catch (Exception e) {
//			throw new Exception();
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * Close.
	 */
	public void close() {
		factory.close();
	}
}
