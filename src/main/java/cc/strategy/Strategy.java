package cc.strategy;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

import cc.beans.BrokerHandler;
import cc.listener.MarketDataHandler;
import cc.model.BollingerBands;
import cc.model.Model;
import cc.model.Model.Decision;
import cc.model.PriceBreakout;
import cc.model.TwoMA;
import cc.monitors.StrategyMonitor;
import cc.services.DatabaseMethods;
import cc.stock.Stock;
import cc.strategy.Trade.brokerResponse;

/**
 * The Class Strategy.
 * 
 * author: Jack Guo, Marco Martin
 */
@Entity(name = "Strategy")
@Table(name = "Strategy")
public class Strategy implements Observer {

	/** The id. */
	@Id
	@Column(name = "Strategy_ID")
	@GeneratedValue
	@Expose
	private String id;

	/**
	 * The Enum StrategyType.
	 */

	public enum StrategyType {

		/** The Two MA. */
		TwoMA,
		/** The Bollinger bands. */
		BollingerBands,
		/** The Price breakout. */
		PriceBreakout
	}

	/**
	 * The Enum Status.
	 */
	public enum Status {

		/** The running. */
		RUNNING,
		/** The stopped. */
		STOPPED
	}

	/** The type. */
	@Enumerated(EnumType.STRING)
	@Column(name = "Type")
	@Expose
//	@Transient
	private StrategyType type;

	/** The status. */
	@Enumerated(EnumType.STRING)
	@Column(name = "Status")
//	@Transient
	private Status status;

	/** The stock name. */
	@Column(name = "Stock")
	@Expose
//	@Transient
	private String stockName;

	/** The stopping threshold. */
	@Column(name = "Threshold")
	@Expose
//	@Transient
	private double stoppingThreshold;

	/** The param 1. */
	@Column(name = "Param1")
	@Expose
	private double param1;

	/** The param 2. */
	@Column(name = "Param2")
	@Expose
	private double param2;

	/** The exit threshold. */
	@Column(name = "Exit_Threshold")
	@Expose
//	@Transient
	private double exitThreshold;

	/** The amount. */
//	@Transient
	@Column(name = "Amount")
	@Expose
	private int amount;

	/** The strategy parameters. */
	@Transient
	private List<Double> strategyParameters;

	/** The trades. */
	@Transient
	private HashMap<String, Trade> trades;

	/** The model. */
	@Transient
	private Model model;

	/** The executed trades. */
	@Transient
	private Queue<Trade> executedTrades;

	/** The run. */
	@Transient
	private boolean run;

	/** The cost to buy. */
	@Column(name = "Cost", precision = 10, scale = 2)
//	@Transient
	private double costToBuy;

	/** The gain of sell. */
	@Column(name = "Profit", precision = 10, scale = 2)
//	@Transient
	private double gainOfSell;

	/** The profit or loss. */
	@Column(name = "profitOrLoss", precision = 10, scale = 2)
	private double profitOrLoss;

	/** The roi. */
	@Column(name = "ROI", precision = 10, scale = 2)
//	@Transient
	private double roi;

	/** The dm. */
	@Transient
	private DatabaseMethods dm;

	/** The bh. */
	@Transient
	private BrokerHandler bh;

	/** The curr stock. */
	@Transient
	private Stock currStock;

	/** The is open. */
	@Transient
	private boolean isOpen;

	/** The amount left. */
	@Transient
	private int amountLeft;

	/** The high. */
	@Transient
	private double high;

	/** The low. */
	@Transient
	private double low;

	/** The tag. */
	@Transient
	private int tag = 0;

	/** The Constant LOGGER. */
	@Transient
	private final static Logger LOGGER = Logger.getLogger(StrategyMonitor.class.getName());

	/** The fh. */
	@Transient
	private FileHandler fh;

	/**
	 * Instantiates a new strategy.
	 */
	public Strategy() {
	}

	/**
	 * Instantiates a new strategy.
	 *
	 * @param id                the id
	 * @param type              the type
	 * @param stockName         the stock name
	 * @param stratParameters   the strat parameters
	 * @param stoppingThreshold the stopping threshold
	 * @param exitThreshold     the exit threshold
	 * @param amount            the amount
	 * @throws Exception the exception
	 */
	public Strategy(String id, String type, String stockName, List<Double> stratParameters, double stoppingThreshold,
			double exitThreshold, int amount) throws Exception {

		this.id = id;
		this.stockName = stockName;
		this.subscribeStrategy();
		this.status = Status.RUNNING;
		try {
			fh = new FileHandler(this.id + ".log");
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LOGGER.setUseParentHandlers(false);
		LOGGER.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();

		fh.setFormatter(formatter);
		this.currStock = MarketDataHandler.getStockFromAPI(stockName);

		this.strategyParameters = (LinkedList<Double>) stratParameters;
		this.trades = new HashMap<String, Trade>();
		this.executedTrades = new LinkedList<Trade>();

		this.stoppingThreshold = stoppingThreshold;
		this.exitThreshold = exitThreshold;
		this.amount = amount;
		setModel(type);

		this.isOpen = false;
		this.costToBuy = 0.0;
		this.gainOfSell = 0.0;
		this.profitOrLoss = 0.0;
		this.roi = 0.0;
		this.stoppingThreshold = stoppingThreshold;

		this.amountLeft = 0;
		this.high = 0.0;
		this.low = 0.0;
		this.bh = new BrokerHandler();
		this.run = false;
		this.tag = 0;

		parseStratParams();
		System.out.println("=================Constructor===============");
		System.out.println(this.type);
		System.out.println(this.exitThreshold);
		System.out.println(this.stoppingThreshold);
		System.out.println(this.amount);
		dm = DatabaseMethods.getInstance();
	}

	public Strategy(Strategy s) throws Exception {
		this.id = s.getId();
		this.stockName = s.getStockName();
		try {
			fh = new FileHandler(this.id + ".log");
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LOGGER.setUseParentHandlers(false);
		LOGGER.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();

		fh.setFormatter(formatter);
		this.currStock = MarketDataHandler.getStockFromAPI(stockName);
		this.strategyParameters = new LinkedList<Double>();
		this.param1 = s.getParams1();
		this.param2 = s.getParams2();
		this.strategyParameters.add(param1);
		this.strategyParameters.add(param2);
		
		this.trades = new HashMap<String, Trade>();
		this.executedTrades = new LinkedList<Trade>();

		this.stoppingThreshold = s.getThreshold();
		this.exitThreshold = s.getExitThreshold();
		this.amount = s.getAmount();
		setModel(s.getType().toString());

		this.isOpen = false;
		this.costToBuy = 0.0;
		this.gainOfSell = 0.0;
		this.profitOrLoss = s.getProfitOrLoss();
		this.roi = s.getROI();

		this.amountLeft = 0;
		this.high = 0.0;
		this.low = 0.0;
		this.bh = new BrokerHandler();
		this.run = false;

		System.out.println("=================Object Constructor===============");
		System.out.println(this.type);
		System.out.println(this.exitThreshold);
		System.out.println(this.stoppingThreshold);
		System.out.println(this.amount);
		dm = DatabaseMethods.getInstance();
	}
	
	public double getExitThreshold() {
		return this.exitThreshold;
	}
	public double getParams1() {
		return this.param1;
	}
	
	public double getParams2() {
		return this.param2;
	}
	public void setModel(String type2) {
		System.out.println(type2);
		switch (type2.toLowerCase()) {
		case "twoma":
			this.type = StrategyType.TwoMA;
			this.model = new TwoMA();
			break;
		case "bollingerbands":
			this.type = StrategyType.BollingerBands;
			this.model = new BollingerBands();
			break;
		case "pricebreakout":
			this.type = StrategyType.PriceBreakout;
			this.model = new PriceBreakout();
			break;
		default:
			this.type = null;
			this.model = null;
			break;
		}
	}
	/**
	 * Persist trades.
	 *
	 * @throws Exception the exception
	 */
	public void persistTrades() throws Exception {
		List<Trade> res = null;
		dm = DatabaseMethods.getInstance();
		trades = new HashMap<String, Trade>();
		String query = "SELECT t FROM Trades t WHERE t.strategyId = '" + this.id + "'";
		try {
			res = dm.executeQuery(query, Trade.class);
			if (res != null) {
				for (Trade t : res) {
					String[] parsedId = t.getId().split("_");
					try {
						int lastTag = Integer.parseInt(parsedId[parsedId.length - 1]);
						if (lastTag > this.tag) {
							this.tag = lastTag + 1;
						}
					} catch (Exception e) {
						LOGGER.log(Level.SEVERE, "Exception occur", e);
					}

					trades.put(t.getId(), t);
				}
			}
			LOGGER.info(this.trades.size() + " Trades loaded from database.");

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
			e.printStackTrace();
		}
		System.out.println("Current tag" + this.tag);
	}

	/**
	 * Parses the strat params.
	 *
	 * @throws Exception the exception
	 */
	public void parseStratParams() throws Exception {
		if (this.strategyParameters.size() != 2) {
			throw new Exception("Bad parameters format");
		}
		this.param1 = this.strategyParameters.get(0);
		this.param2 = this.strategyParameters.get(1);
	}

	/**
	 * Make trade.
	 *
	 * @throws Exception the exception
	 */
	// For testing, remove later
	public void makeTrade() throws Exception {
		Double currPrice = this.currStock.getClose().get(this.currStock.getClose().size() - 1);
		LocalDateTime currTimestamp = this.currStock.getTimeStamp().get(this.currStock.getTimeStamp().size() - 1);
		String tradeId = this.id + "_" + this.stockName + "_" + tag;
//		System.out.println(tradeId);
		Decision decision = Decision.BUY;
		Trade trade = new Trade(tradeId, this.type, this.stockName, this.amount, currPrice, decision, currTimestamp,
				this.id);
		trades.put(tradeId, trade);
		bh.makeTrade(trade);
		tag++;
		Thread.sleep(14000);
		System.out.println("Executed trades queue size:" + executedTrades.size());
	}

	/**
	 * Run.
	 *
	 * @throws Exception the exception
	 */
	// start the current strategy
	public void run() throws Exception {
		System.out.println(this.model);
		if (this.model == null) {
			throw new Exception("No such model");
		}
		this.status = Status.RUNNING;

		Double currPrice = this.currStock.getClose().get(this.currStock.getClose().size() - 1);
		LocalDateTime currTimestamp = this.currStock.getTimeStamp().get(this.currStock.getTimeStamp().size() - 1);

		checkResponses(currPrice);
		if (!isOpen) {
			Decision decision = getDecision();
			if (decision.equals(Decision.BUY) || decision.equals(Decision.SELL)) {

				String tradeId = this.id + "_" + this.stockName + "_" + tag;

				Trade trade = new Trade(tradeId, this.type, this.stockName, this.amount, currPrice, decision,
						currTimestamp, this.id);

				trades.put(tradeId, trade);
				try {
					dm.writeToDatabase(trade);
				} catch (Exception e) {
					LOGGER.log(Level.SEVERE, "Exception occur", e);
					System.out.println("Trade already exists in DB");
				}

				bh.makeTrade(trade);
				LOGGER.info("Trade " + tradeId + " created and sent to market.");
				tag++;
				Thread.sleep(14000);

				checkResponses(currPrice);
			}

		} else {
			System.out.println("Position is open");
			Decision prevDecision = trades.get(this.id + "_" + this.stockName + "_" + (tag - 1)).getDecision();
			brokerResponse prevResponse = trades.get(this.id + "_" + this.stockName + "_" + (tag - 1)).getResponse();
			Decision currDecision = null;
			if (prevDecision.equals(Decision.SELL)) {
				if (prevResponse == null) {
					currDecision = Decision.SELL;
				} else if (prevResponse.equals(brokerResponse.FILLED)) {
					currDecision = Decision.BUY;
				} else if (prevResponse.equals(brokerResponse.PARTIAL_FILLED)) {
					currDecision = Decision.SELL;
				}
			} else {
				currDecision = Decision.SELL;
			}

			if (currPrice >= high || currPrice <= low) {
				String tradeId = this.id + "_" + this.stockName + "_" + tag;

				Trade trade = new Trade(tradeId, this.type, this.stockName, this.amount, currPrice, currDecision,
						currTimestamp, this.id);

				trades.put(tradeId, trade);
				try {
					dm.writeToDatabase(trade);
				} catch (Exception e) {
					LOGGER.log(Level.SEVERE, "Exception occur", e);
					System.out.println("Trade already exists in DB");
				}

//				mockMakeFilledTrade(trade);
				bh.makeTrade(trade);

				tag++;
				isOpen = false;
				Thread.sleep(14000);
			}

		}
//		testCounter++;
//		if (testCounter == 3) {
//			this.run = false;
//		}
//		System.out.println(testCounter);

		System.out.println(this.profitOrLoss);
	}
	/**
	 * Check responses.
	 *
	 * @param currPrice the curr price
	 * @throws Exception the exception
	 */
	private void checkResponses(Double currPrice) throws Exception {
		// When we get a response
		if (!this.executedTrades.isEmpty()) {
			try {
				System.out.println("Evaluating Performance");
				while (!executedTrades.isEmpty()) {
					Trade executedTrade = this.executedTrades.remove();

					brokerResponse tradeStatus = executedTrade.getResponse();
					executedTrade.setBrokerResponse(tradeStatus);

					if (tradeStatus.equals(brokerResponse.FILLED)
							|| tradeStatus.equals(brokerResponse.PARTIAL_FILLED)) {
						if (executedTrade.isBuy()) {
							this.costToBuy += executedTrade.getAmount() * executedTrade.getPrice();
						} else {
							this.gainOfSell += executedTrade.getAmount() * executedTrade.getPrice();
						}

						this.profitOrLoss = Double
								.parseDouble(new DecimalFormat(".##").format(this.gainOfSell - this.costToBuy));
						try {
							this.roi = Double.parseDouble(new DecimalFormat(".##")
									.format((this.gainOfSell - this.costToBuy) / this.costToBuy));
						} catch (Exception e) {
							this.roi = 0.0;
						}
						executedTrade.setProfitOrLoss(this.profitOrLoss);
						executedTrade.setROI(this.roi);

						try {
							dm.mergeToDatabase(executedTrade);
						} catch (Exception e) {
							LOGGER.log(Level.SEVERE, "Exception occur", e);
//							System.out.println("Trade already in database");
						}
						isOpen = true;
					}

					if (tradeStatus.equals(brokerResponse.PARTIAL_FILLED)) {
						amountLeft = this.amount - executedTrade.getAmount();
						high = currPrice + currPrice * this.stoppingThreshold;
						low = currPrice - currPrice * this.stoppingThreshold;
					}
					LOGGER.info("Trade " + executedTrade.getId() + " performance calculated.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the decision.
	 *
	 * @return the decision
	 */
	public Decision getDecision() {
//		System.out.println(this.model);
		return this.model.getDecision(this.strategyParameters, this.currStock);
	}

	/**
	 * Put executed trade.
	 *
	 * @param trade the trade
	 */
	public void putExecutedTrade(Trade trade) {
		this.executedTrades.add(trade);
	}

	/**
	 * Start.
	 */
	public void start() {
		this.status = Status.RUNNING;
		subscribeStrategy();
	}

	/**
	 * Pause.
	 */
	// pause the current strategy
	public void pause() {
		this.status = Status.STOPPED;
		unsubscribeStrategy();
		// TODO:Implement how to pause
	}

	/**
	 * Unsubscribe strategy.
	 */
	public void unsubscribeStrategy() {
		MarketDataHandler.unsubscribe(this.stockName, this);
	}

	/**
	 * Subscribe strategy.
	 */
	public void subscribeStrategy() {
		MarketDataHandler.subscribe(this.stockName, this);
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Status getStatus() {
		return this.status;
	}

	/**
	 * Gets the profit or loss history.
	 *
	 * @return the profit or loss history
	 * @throws Exception the exception
	 */
	// Returns entire history of performance of the instance of this strategy
	@SuppressWarnings("unchecked")
	public List<Double> getProfitOrLossHistory() throws Exception {
		this.dm = DatabaseMethods.getInstance();
		String q = "SELECT t.profitOrLoss FROM Trades t WHERE t.strategyId = :placeholder";
		return dm.executeQuery(q, this.id, Trade.class);
	}

	/**
	 * Gets the profit or loss at time.
	 *
	 * @param t the t
	 * @return the profit or loss at time
	 * @throws Exception the exception
	 */
	// Return specific performance at some time t
	@SuppressWarnings("unchecked")
	public LinkedList<Double> getProfitOrLossAtTime(Timestamp t) throws Exception {
		this.dm = DatabaseMethods.getInstance();
		String q = "SELECT profitOrLoss FROM Trades WHERE Timestamp = :placeholder";
		return (LinkedList<Double>) dm.executeQuery(q, t, Trade.class);
	}

	/**
	 * Gets the ROI history.
	 *
	 * @return the ROI history
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public LinkedList<Double> getROIHistory() throws Exception {
		this.dm = DatabaseMethods.getInstance();
		String q = "SELECT ROI FROM Trades WHERE t.strategyId = :placeholder";
		return (LinkedList<Double>) dm.executeQuery(q, this.id, Trade.class);
	}

	/**
	 * Gets the ROI at time.
	 *
	 * @param t the t
	 * @return the ROI at time
	 * @throws Exception the exception
	 */
	// Return specific performance at some time t
	@SuppressWarnings("unchecked")
	public LinkedList<Double> getROIAtTime(Timestamp t) throws Exception {
		this.dm = DatabaseMethods.getInstance();
		String q = "SELECT ROI FROM Trades WHERE Timestamp = :placeholder";
		return (LinkedList<Double>) dm.executeQuery(q, t, Trade.class);
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public StrategyType getType() {
		return type;
	}

	/**
	 * Gets the stock name.
	 *
	 * @return the stock name
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * Gets the strategy parameters.
	 *
	 * @return the strategy parameters
	 */
	public List<Double> getStrategyParameters() {
		return strategyParameters;
	}

	/**
	 * Gets the executed trades.
	 *
	 * @return the executed trades
	 */
	public Queue<Trade> getExecutedTrades() {
		return this.executedTrades;
	}

	/**
	 * Gets the executed trades size.
	 *
	 * @return the executed trades size
	 */
	public int getExecutedTradesSize() {
		return this.executedTrades.size();
	}

	/**
	 * Gets the trade.
	 *
	 * @param id the id
	 * @return the trade
	 */
	public Trade getTrade(String id) {
		return this.trades.get(id);
	}

	/**
	 * Gets the trades.
	 *
	 * @return the trades
	 */
	public HashMap<String, Trade> getTrades() {
		return this.trades;
	}

	/**
	 * Gets the cost.
	 *
	 * @return the cost
	 */
	public double getCost() {
		return this.costToBuy;
	}

	/**
	 * Gets the profit.
	 *
	 * @return the profit
	 */
	public double getProfit() {
		return this.gainOfSell;
	}

	/**
	 * Gets the roi.
	 *
	 * @return the roi
	 */
	public double getROI() {
		return this.roi;
	}

	/**
	 * Gets the profit or loss.
	 *
	 * @return the profit or loss
	 */
	public double getProfitOrLoss() {
		return this.profitOrLoss;
	}

	/**
	 * Gets the threshold.
	 *
	 * @return the threshold
	 */
	public double getThreshold() {
		return this.stoppingThreshold;
	}

	/**
	 * Update stock.
	 *
	 * @param newStock the new stock
	 * @throws Exception the exception
	 */
	public void updateStock(Stock newStock) throws Exception {
		this.currStock = newStock;
		run();
//		makeTrade();
	}

	public int getAmount() {
		return this.amount;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub

	}

}
