package cc.strategy;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import cc.model.Model.Decision;
import cc.strategy.Strategy.StrategyType;

/**
 * The Class Trade.
 * 
 * author: Jack Guo, Marco Martin
 */
@Entity(name = "Trades")
@Table(name = "Trades")
public class Trade {
	
	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Trade_ID", updatable = false, nullable = false)
	private String id;

	/** The strategy type. */
	@Enumerated(EnumType.STRING)
	@Column(name = "StrategyType", length = 20)
//	@Transient
	private StrategyType strategyType;

	/** The stock name. */
	@Column(name = "Stock")
//	@Transient
	private String stockName;

	/** The amount. */
	@Column(name = "Amount")
//	@Transient
	private int amount;

	/** The decision. */
	@Enumerated(EnumType.STRING)
	@Column(name = "Decision", length = 20)
//	@Transient
	private Decision decision;

	/** The price. */
	@Column(name = "Price")
//	@Transient
	private double price;

	/** The timestamp. */
	@Column(name = "Timestamp")
//	@Transient
	private LocalDateTime timestamp;

	/** The profit or loss. */
	@Column(name = "ProfitOrLoss")
//	@Transient
	private double profitOrLoss;

	/** The roi. */
	@Column(name = "ROI")
//	@Transient
	private double ROI;

	/**
	 * The Enum brokerResponse.
	 */
	public enum brokerResponse {
		
		/** The filled. */
		FILLED, 
 /** The partial filled. */
 PARTIAL_FILLED, 
 /** The rejected. */
 REJECTED
	}

	/** The response. */
	@Enumerated(EnumType.STRING)
	@Column(name = "Broker_Response")
//	@Transient
	private brokerResponse response;

	/** The strategy id. */
	@Column(name = "strategyId")
	private String strategyId;

	/**
	 * Instantiates a new trade.
	 */
	public Trade() {
	}

	/**
	 * Instantiates a new trade.
	 *
	 * @param id the id
	 * @param strategyType the strategy type
	 * @param stockName the stock name
	 * @param amount the amount
	 * @param price the price
	 * @param decision the decision
	 * @param timestamp the timestamp
	 * @param strategyId the strategy id
	 * @throws Exception the exception
	 */
	public Trade(String id, StrategyType strategyType, String stockName, int amount, double price, Decision decision,
			LocalDateTime timestamp, String strategyId) throws Exception {
		this.id = id;
		this.strategyType = strategyType;
		this.stockName = stockName;
		this.amount = amount;
		this.price = price;
		this.decision = decision;
		this.timestamp = timestamp;
		this.profitOrLoss = 0.0;
		this.ROI = 0.0;
		this.strategyId = strategyId;
	}

	/**
	 * Gets the response.
	 *
	 * @return the response
	 */
	public brokerResponse getResponse() {
		return this.response;
	}

	/**
	 * Instantiates a new trade.
	 *
	 * @param responseXml the response xml
	 * @param correlationID the correlation ID
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Trade(String responseXml, String correlationID)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(responseXml));

		Document doc = builder.parse(src);
		this.stockName = doc.getElementsByTagName("stock").item(0).getTextContent();
		this.id = correlationID;
		this.strategyId = this.id.split("_")[0];
		this.amount = Integer.parseInt(doc.getElementsByTagName("size").item(0).getTextContent());
		this.price = Double.parseDouble(doc.getElementsByTagName("price").item(0).getTextContent());
		this.decision = toDecision(doc.getElementsByTagName("buy").item(0).getTextContent());
		this.response = toBrokerResponse(doc.getElementsByTagName("result").item(0).getTextContent());
//		System.out.println("ERHWFHFGHSFDHFHSFHRFHERG");
		// Strip 'T' and 'Z' from time
		String s = doc.getElementsByTagName("whenAsDate").item(0).getTextContent();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		this.timestamp = LocalDateTime.parse(s, formatter);
		System.out.println(this.stockName + ":" + this.id + ":" + this.amount + ":" + this.price + ":"
				+ this.decision.toString() + ":" + this.timestamp);
	}

	/**
	 * To broker response.
	 *
	 * @param value the value
	 * @return the broker response
	 */
	private brokerResponse toBrokerResponse(String value) {
		switch (value.toLowerCase()) {
		case "filled":
			return brokerResponse.FILLED;
		case "partial_filled":
			return brokerResponse.PARTIAL_FILLED;
		default:
			return brokerResponse.REJECTED;
		}
	}

	/**
	 * To decision.
	 *
	 * @param value the value
	 * @return the decision
	 */
	private Decision toDecision(String value) {
		switch (value.toLowerCase()) {
		case "true":
			return Decision.BUY;
		case "false":
			return Decision.SELL;
		default:
			return Decision.DO_NOTHING;
		}
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the strategy type.
	 *
	 * @return the strategy type
	 */
	public StrategyType getStrategyType() {
		return this.strategyType;
	}

	/**
	 * Gets the stock name.
	 *
	 * @return the stock name
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public int getAmount() {
		return this.amount;
	}

	/**
	 * Gets the decision.
	 *
	 * @return the decision
	 */
	public Decision getDecision() {
		return decision;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	/**
	 * Checks if is buy.
	 *
	 * @return true, if is buy
	 */
	public boolean isBuy() {
		return decision == Decision.BUY;
	}

	/**
	 * Sets the profit or loss.
	 *
	 * @param profitOrLoss the new profit or loss
	 */
	public void setProfitOrLoss(double profitOrLoss) {
		this.profitOrLoss = profitOrLoss;
	}

	/**
	 * Sets the roi.
	 *
	 * @param ROI the new roi
	 */
	public void setROI(double ROI) {
		this.ROI = ROI;
	}

	/**
	 * Gets the strategy id.
	 *
	 * @return the strategy id
	 */
	public String getStrategyId() {
		return this.strategyId;
	}

	/**
	 * Gets the roi.
	 *
	 * @return the roi
	 */
	public double getROI() {
		return this.ROI;
	}

	/**
	 * Sets the broker response.
	 *
	 * @param br the new broker response
	 */
	public void setBrokerResponse(brokerResponse br) {
		this.response = br;
	}

	/**
	 * Sets the amount.
	 *
	 * @param d the new amount
	 */
	// For testing, remove later
	public void setAmount(double d) {
		this.amount = (int) d;
	}
}
