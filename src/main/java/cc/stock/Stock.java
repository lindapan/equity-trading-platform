package cc.stock;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class Stock.
 * 
 * author: Jack Guo
 */
public class Stock {
	
	/** The listing. */
	private String listing;
	
	/** The high price. */
	private List<Double> highPrice;
	
	/** The low price. */
	private List<Double> lowPrice;
	
	/** The curr price. */
	private List<Double> currPrice;
	
	/** The open. */
	private List<Double> open;
	
	/** The close. */
	private List<Double> close;
	
	/** The volume. */
	private List<Integer> volume;
	
	/** The time stamp. */
	private List<LocalDateTime> timeStamp;

	/**
	 * Instantiates a new stock.
	 */
	public Stock() {

	}

	/**
	 * Instantiates a new stock.
	 *
	 * @param name the name
	 * @param highPrice the high price
	 * @param lowPrice the low price
	 * @param open the open
	 * @param close the close
	 * @param volume the volume
	 * @param timeStamp the time stamp
	 */
	public Stock(String name, List<Double> highPrice, List<Double> lowPrice, List<Double> open, List<Double> close,
			List<Integer> volume, List<LocalDateTime> timeStamp) {
		listing = name;
		this.highPrice = highPrice;
		this.lowPrice = lowPrice;
		this.open = open;
		this.close = close;
		this.volume = volume;
		this.timeStamp = timeStamp;
		this.currPrice = avg(highPrice, lowPrice);
	}

	/**
	 * Avg.
	 *
	 * @param l1 the l 1
	 * @param l2 the l 2
	 * @return the list
	 */
	public List<Double> avg(List<Double> l1, List<Double> l2) {
		List<Double> l3 = new LinkedList<Double>();
		try {
			for (int i = 0; i < l1.size(); i++) {
				l3.add((l1.get(i)+l2.get(i))/2.0);
			}
		} catch (Exception e) {

		}
		return l3;

	}
	
	/**
	 * Gets the curr price.
	 *
	 * @return the curr price
	 */
	public List<Double> getCurrPrice(){
		return this.currPrice;
	}
	
	/**
	 * Gets the listing.
	 *
	 * @return the listing
	 */
	public String getListing() {
		return listing;
	}

	/**
	 * Gets the high price.
	 *
	 * @return the high price
	 */
	public List<Double> getHighPrice() {
		return highPrice;
	}

	/**
	 * Gets the low price.
	 *
	 * @return the low price
	 */
	public List<Double> getLowPrice() {
		return lowPrice;
	}

	/**
	 * Gets the open.
	 *
	 * @return the open
	 */
	public List<Double> getOpen() {
		return open;
	}

	/**
	 * Gets the close.
	 *
	 * @return the close
	 */
	public List<Double> getClose() {
		return close;
	}

	/**
	 * Gets the volume.
	 *
	 * @return the volume
	 */
	public List<Integer> getVolume() {
		return volume;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public List<LocalDateTime> getTimeStamp() {
		return timeStamp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		System.out.println("Stock name:" + this.listing);
		System.out.println("Current window size:" + this.highPrice.size());
		System.out.println("Current high prices:");
		for (Double p : highPrice) {
			System.out.println(p);
		}
		return "";
	}

}
