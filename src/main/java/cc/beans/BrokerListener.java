package cc.beans;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import cc.monitors.StrategyMonitor;
import cc.strategy.Strategy;
import cc.strategy.Trade;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving broker events.
 * The class that is interested in processing a broker
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addBrokerListener<code> method. When
 * the broker event occurs, that object's appropriate
 * method is invoked.
 *
 * @see BrokerEvent
 * 
 * author: Marco Martin
 */
public class BrokerListener implements MessageListener {

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	@Override
	public void onMessage(Message m) {
		TextMessage message = (TextMessage) m;
		try {
//			Enumeration<String> myEnum = (Enumeration<String>) message.getMapNames();
//			String s = null;
//			while(myEnum.hasMoreElements()) {
//				s += myEnum.nextElement();
//				 System.out.println(s);
//			}
			// Extract the id of the strategy used to make this trade
			String responseXml = message.getText();
//			System.out.println("akdjfksdjfkasdjf"+m.getJMSCorrelationID());
			Trade tradeResponse = new Trade(responseXml, m.getJMSCorrelationID());
//			String strategyId = tradeResponse.getStrategy2Id();
			String strategyId = m.getJMSCorrelationID().split("_")[0];
			String err = "";
			try {
				// Find the stratgy object that called this trade
				System.out.println("Putting trade into strategy "+strategyId);
				StrategyMonitor sm = StrategyMonitor.getInstance();
				Strategy s = sm.getStrategy(m.getJMSCorrelationID().split("_")[0]);
				s.putExecutedTrade(tradeResponse);	
			}catch(NullPointerException e) {
				err+="Cannot find strategy "+strategyId+" for trade with ID "+tradeResponse.getId();
			}

			// Log the response from the server
			BufferedWriter writer = new BufferedWriter(new FileWriter("BrokerResponseLog.txt"));
			writer.write(responseXml + "\r\n");
			writer.write(err);
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
