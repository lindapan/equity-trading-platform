package cc.model;

import java.util.LinkedList;
import java.util.List;

import cc.stock.Stock;
import cc.strategy.Strategy.StrategyType;

/**
 * The Class PriceBreakout.
 * 
 * author: Jack Guo
 */
public class PriceBreakout implements Model {

	/* (non-Javadoc)
	 * @see cc.model.Model#getDecision(java.util.List, cc.stock.Stock)
	 */
	@Override
	public Decision getDecision(List<Double> parameters, Stock stock) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see cc.model.Model#getName()
	 */
	public StrategyType getName() {
		return StrategyType.PriceBreakout;
	}

}
