package cc.model;

import java.util.LinkedList;
import java.util.List;

import cc.stock.Stock;
import cc.strategy.Strategy.StrategyType;

/**
 * The Class TwoMA.
 * 
 * author: Jack Guo
 */
public class TwoMA implements Model {

	/** The long window. */
	private double longWindow;
	
	/** The short window. */
	private double shortWindow;
	
	/** The prices. */
	private LinkedList<Double> prices; 
	
	/**
	 * The Enum flag.
	 */
	private enum flag {
		
		/** The init. */
		INIT, 
 /** The short. */
 SHORT, 
 /** The long. */
 LONG
	}
	
	/** The type. */
	//TODO: figure out how to keep track of flag
	private flag type = flag.INIT;
	
	/**
	 * Instantiates a new two MA.
	 */
	public TwoMA() {
		this.longWindow = 0.0;
		this.shortWindow = 0.0;
		this.prices = null;
		this.type = flag.INIT;
	}


	/* (non-Javadoc)
	 * @see cc.model.Model#getDecision(java.util.List, cc.stock.Stock)
	 */
	public Decision getDecision(List<Double> parameters, Stock stock) {
		// TODO Auto-generated method stub
		this.prices = (LinkedList<Double>) stock.getClose();
		try {
			parseParameters(parameters);
			Decision decision = null;
			double shortAverage = calcMA(shortWindow, prices);
			double longAverage = calcMA(longWindow, prices);

			if (shortAverage < 0 || longAverage < 0) {

				decision = Decision.DO_NOTHING;
			}

			if (type.equals(flag.INIT)) {
				if (shortAverage > longAverage) {
					type = flag.SHORT;
					decision = Decision.BUY;
				} else {
					type = flag.LONG;
					decision = Decision.SELL;
				}
			} else {
				if (longAverage > shortAverage) {
					if (type.equals(flag.LONG)) {
						decision = Decision.DO_NOTHING;
					} else {
						type = flag.LONG;
						decision = Decision.SELL;
					}

				} else {
					if (type.equals(flag.SHORT)) {
						decision = Decision.DO_NOTHING;

					} else {
						type = flag.SHORT;
						decision = Decision.BUY;
					}
				}

			}
			return decision;
		} catch (Exception e) {
			e.printStackTrace();
//			return Decision.DO_NOTHING;
			return null;
		}

	}

	/* (non-Javadoc)
	 * @see cc.model.Model#getName()
	 */
	public StrategyType getName() {
		return StrategyType.TwoMA;
	}
	
	/**
	 * Sets the flag.
	 *
	 * @param f the new flag
	 */
	public void setFlag(flag f) {
		this.type = f; 
		
	}
	
	/**
	 * Parses the parameters.
	 *
	 * @param parameters the parameters
	 * @throws Exception the exception
	 */
	private void parseParameters(List<Double> parameters) throws Exception {
		if (parameters.size() != 2) {
			throw new Exception("Bad parameters format");
		}
		try {
			this.longWindow = parameters.get(0);
			this.shortWindow = parameters.get(1);
		} catch (Exception e) {

		}
	}

	/**
	 * Calc MA.
	 *
	 * @param period the period
	 * @param dataStorage the data storage
	 * @return the double
	 */
	private double calcMA(double period, LinkedList<Double> dataStorage) {
		try {
			Double count = (double) 0;
			for (int i = 0; i < period; i++) {
				count += ((LinkedList<Double>) dataStorage).get(i);
			}
			return Math.round((count / period) * 10.0) / 10.0;
		} catch (Exception e) {
			System.out.println("Average has been asked too soon");
			return -1;
		}
	}


}
