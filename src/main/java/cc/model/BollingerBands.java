package cc.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cc.model.Model.Decision;
import cc.stock.Stock;
import cc.strategy.Strategy.StrategyType;

/**
 * The Class BollingerBands.
 * 
 * author: Linda Pan
 */
public class BollingerBands implements Model {
	
	/** The window. */
	private double window;
	
	/** The deviation. */
	private double deviation;
	
	/** The prices. */
	private LinkedList<Double> prices;

	/* (non-Javadoc)
	 * @see cc.model.Model#getDecision(java.util.List, cc.stock.Stock)
	 */
	public Decision getDecision(List<Double> parameters, Stock stock) {
		this.prices = (LinkedList<Double>) stock.getClose();
		try {
			List<Double> band = new ArrayList<>();
			Decision decision = null;
			parseParameters(parameters);
			calcBB(window, prices);
			band = new ArrayList<Double>(calcBB(window, prices));
			Double upper = band.get(0);
			Double lower = band.get(1);
			Double currentPrice = prices.getLast();
			
			if(upper <= lower ) {
				decision = Decision.DO_NOTHING;
			}
			if (currentPrice > upper) {
				decision = Decision.SELL;
			} else if (currentPrice < lower) {
				decision = Decision.BUY;
			} else {
				decision = Decision.DO_NOTHING;
			}
			return decision;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Decision.DO_NOTHING;
	}

	/* (non-Javadoc)
	 * @see cc.model.Model#getName()
	 */
	public StrategyType getName() {
		return StrategyType.BollingerBands;
	}
	
	/**
	 * Parses the parameters.
	 *
	 * @param parameters the parameters
	 * @throws Exception the exception
	 */
	private void parseParameters(List<Double> parameters) throws Exception {
		if (parameters.size() != 2) {
			throw new Exception("Bad parameters format");
		}
		try {
			this.window = parameters.get(0);
			this.deviation  = parameters.get(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Calc BB.
	 *
	 * @param period the period
	 * @param dataStorage the data storage
	 * @return the array list
	 */
	private ArrayList<Double> calcBB(Double period, List<Double> dataStorage) {
		
		List<Double> result = new ArrayList<>();
		List<Double> tmp = new ArrayList<>();
		
		try {
			Double sum = 0.0, stdDev = 0.0, SMA = 0.0;
			
			for(int i = 0; i < period; i++) {
				sum += dataStorage.get(i);
				tmp.add(dataStorage.get(i));
			}
			
			SMA = Math.round((sum / period) * 10.0) / 10.0;
			stdDev =  getStdDev(period, tmp);
	        result.add(SMA + stdDev);
	        result.add(SMA - stdDev);
	        return (ArrayList<Double>) result;
			
		} catch (Exception e) {
			System.out.println("Average has been asked too soon");
			result.add(0.0);
			result.add(0.0);
			return (ArrayList<Double>) result;
		}
	}

	/**
	 * Gets the std dev.
	 *
	 * @param period the period
	 * @param data the data
	 * @return the std dev
	 */
	private Double getStdDev(Double period, List<Double> data) {
		
		// Step 1: Find the mean
	    Double mean = mean(data);
	    Double temp = 0.0;

	    for (int i = 0; i < data.size(); i++)
	    {
	        Double val = data.get(i);

	        // Step 2: For each data point, find the square of its distance to the mean.
	        double squrDiffToMean = Math.pow(val - mean, 2);

	        // Step 3: Sum the values from Step 2.
	        temp += squrDiffToMean;
	    }
	    // Step 4: Divide by the number of data points.
	    double meanOfDiffs = (double) temp / (double) (data.size());
	    
	    // Step 5: Take the square root.
	    return Math.sqrt(meanOfDiffs);
	}
	
	 /**
 	 * Mean.
 	 *
 	 * @param data the data
 	 * @return the double
 	 */
 	private Double mean (List<Double> data)
	    {
	        Double sum = 0.0;

	        for (int i= 0;i < data.size(); i++)
	        {
	            Double currentNum = data.get(i);
	            sum+= currentNum;
	        }
	        return Math.round((sum / data.size()) * 10.0) / 10.0;
	    }

}
