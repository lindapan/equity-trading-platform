package cc.model;

import java.util.List;

import cc.stock.Stock;
import cc.strategy.Strategy.StrategyType;

/**
 * The Interface Model.
 * 
 * author: Jack Guo
 */
public interface Model {
	
	/**
	 * The Enum Decision.
	 */
	public enum Decision {
		
		/** The buy. */
		BUY, 
 /** The sell. */
 SELL, 
 /** The do nothing. */
 DO_NOTHING
	}
	
	/**
	 * Gets the decision.
	 *
	 * @param strategyParameters the strategy parameters
	 * @param stock the stock
	 * @return the decision
	 */
	public Decision getDecision(List<Double> strategyParameters, Stock stock);
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public StrategyType getName();

}
