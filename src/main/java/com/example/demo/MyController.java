package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The Class MyController.
 * 
 * author: Linda Pan
 */
@Controller
public class MyController {

    /**
     * Handler.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping("/")
    public String handler(Model model) {
        model.addAttribute("msg", "Spring Boot web app, JAR packaging, JSP views");
        return "myView";
    }
}