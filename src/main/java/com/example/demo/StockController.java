package com.example.demo;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import cc.listener.MarketDataHandler;
import cc.monitors.StockMonitor;
import cc.monitors.StrategyMonitor;
import cc.strategy.Strategy;

// TODO: Auto-generated Javadoc
/**
 * The Class StockController.
 *
 * @author haoyuepan
 */

@RestController

public class StockController {
	
	/**
	 * Handler.
	 *
	 * @return the string
	 */
	@RequestMapping("/hello")
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String handler() {
			
		System.out.println("hello");
	    return "{ \"name\":\"John\", \"age\":30, \"car\":null }";
	}
	
	
	/**
	 * Gets the stock.
	 *
	 * @param stockName the stock name
	 * @return the stock
	 */
	@RequestMapping(value = "/getStock", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String getStock(@RequestBody String stockName) {
		System.out.println(stockName);
		
		StockMonitor sm = StockMonitor.getInstance();
		sm.addStock(stockName);
		System.out.println(sm.getStockAsJson(stockName).toString());
		
		return sm.getStockAsJson(stockName).toString();
	}
	

	/**
	 * Adds the stock.
	 *
	 * @param stockName the stock name
	 * @return the string
	 */
	@RequestMapping(value = "/addStock", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String addStock(@RequestBody String stockName) {
		System.out.println(stockName);
		
		StockMonitor sm = StockMonitor.getInstance();
		sm.addStock(stockName);
		System.out.println(sm.getStockAsJson(stockName).toString());
		
		return sm.getStockAsJson(stockName).toString();
	}
	

	/**
	 * Adds the strategy.
	 *
	 * @param list the list
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/addStrategy", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String addStrategy(@RequestBody List<String> list) throws Exception {
		
		
		StrategyMonitor sm = StrategyMonitor.getInstance();
		String response = sm.createStrategy(list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5), list.get(6), list.get(7));
		
		JsonObject json = new JsonObject();
		json.addProperty("status", response);
		
		return json.toString();
		
	}
	
	/**
	 * Start pricing service.
	 *
	 * @param s the s
	 * @return the string
	 */
	@RequestMapping(value = "/startPricingService", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String startPricingService(@RequestBody String s) {
		System.out.println("In the callllllllllllllllllllllll");
		ExecutorService ex = Executors.newSingleThreadExecutor();
		MarketDataHandler mdh = MarketDataHandler.getInstance();
		try {
			ex.execute(mdh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * Gets the running strategy.
	 *
	 * @return the running strategy
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/getRunningStrategy", method = RequestMethod.GET)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String getRunningStrategy() throws Exception {
		
		StrategyMonitor sm = StrategyMonitor.getInstance();

		List<Strategy> rs = sm.getRunningStrategies();
		
		Gson gson = new GsonBuilder()
	            .excludeFieldsWithoutExposeAnnotation()
	            .create();
	    String data = gson.toJson(rs);

		return data;
		
	}
	

	@RequestMapping(value = "/getPerformance", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String getPerformance(@RequestBody String s) throws Exception {
		
		StrategyMonitor sm = StrategyMonitor.getInstance();
	
		
		String pnl = sm.getProfitOrLossFromStrategy(s);
//		String roi = sm.getROIFromStrategy(s);
		System.out.println("Front end:"+pnl);
//		System.out.println("Front end:"+roi);
		
	    
	    
	    JsonObject json = new JsonObject();
		json.addProperty("pnl", pnl);
//		json.addProperty("roi", roi);
		
		return json.toString();

		
	}
	
	
	/**
	 * Gets the paused strategy.
	 *
	 * @return the paused strategy
	 */

	@RequestMapping(value = "/getPausedStrategy", method = RequestMethod.GET)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String getPausedStrategy() {
		
		StrategyMonitor sm = StrategyMonitor.getInstance();
		List<Strategy> rs = sm.getPausedStrategies();
		
		Gson gson = new GsonBuilder()
	            .excludeFieldsWithoutExposeAnnotation()
	            .create();
	    String data = gson.toJson(rs);
//		Gson gson = new Gson();
//		String data = gson.toJson(rs);
		
		return data;

	}
	
	/**
	 * Pause strategy.
	 *
	 * @param id the id
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/pauseStrategy", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String pauseStrategy(@RequestBody String id) throws Exception {
		
		StrategyMonitor sm = StrategyMonitor.getInstance();
		sm.pauseStrategy(id);
		JsonObject json = new JsonObject();
		json.addProperty("status", "success");
		
		return json.toString();
	

	}
	
	/**
	 * Stop strategy.
	 *
	 * @param id the id
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/stopStrategy", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String stopStrategy(@RequestBody String id) throws Exception {
		
		StrategyMonitor sm = StrategyMonitor.getInstance();
		sm.removeStrategy(id);;
		JsonObject json = new JsonObject();
		json.addProperty("status", "success");
		
		return json.toString();
	

	}
	
	/**
	 * Start strategy.
	 *
	 * @param id the id
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/startStrategy", method = RequestMethod.POST)
	@ResponseBody
	@CrossOrigin(origins = "http://localhost:4200")
	public String startStrategy(@RequestBody String id) throws Exception {
		
		StrategyMonitor sm = StrategyMonitor.getInstance();
		sm.startStrategy(id);;
		JsonObject json = new JsonObject();
		json.addProperty("status", "success");
		
		return json.toString();
	
	}

}
