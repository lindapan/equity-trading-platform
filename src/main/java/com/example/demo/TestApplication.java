package com.example.demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cc.listener.MarketDataHandler;
import cc.monitors.StrategyMonitor;


/**
 * The Class TestApplication.
 * 
 * @author Linda Pan
 */
@SpringBootApplication
//@EnableJms
public class TestApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
		
		ExecutorService ex = Executors.newSingleThreadExecutor();
		StrategyMonitor sm = StrategyMonitor.getInstance();
		MarketDataHandler mdh = MarketDataHandler.getInstance();
		try {
			ex.execute(mdh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
